-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 22, 2020 at 10:49 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `we_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

DROP TABLE IF EXISTS `about`;
CREATE TABLE IF NOT EXISTS `about` (
  `Abo_Detail` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`Abo_Detail`, `created_at`, `updated_at`) VALUES
(NULL, '2020-06-09 00:00:00', '2020-06-09 22:17:40');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `Adm_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Adm_Username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adm_Password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adm_Fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adm_Email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adm_Tel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Adm_Department` int(11) NOT NULL DEFAULT 0,
  `Adm_Permission` int(11) NOT NULL DEFAULT 1 COMMENT '0 = ยกเลิก 1 = ใช้งาน',
  `Adm_Status` int(11) NOT NULL DEFAULT 0 COMMENT '0 = admin 1 = super admin',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`Adm_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`Adm_ID`, `Adm_Username`, `Adm_Password`, `Adm_Fullname`, `Adm_Email`, `Adm_Tel`, `Adm_Department`, `Adm_Permission`, `Adm_Status`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'e10adc3949ba59abbe56e057f20f883e', 'Administrator', 'admin@g.com', '1234567890', 0, 1, 1, '2019-09-09 13:47:32', '2020-07-22 15:21:58');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
CREATE TABLE IF NOT EXISTS `blog` (
  `Kno_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Adm_ID` int(11) NOT NULL DEFAULT 0,
  `Gro_ID` int(11) NOT NULL DEFAULT 0,
  `Cat_ID` int(11) NOT NULL DEFAULT 0,
  `Sec_ID` int(11) DEFAULT 0,
  `Kno_Thumbnail` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kno_Title` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kno_ShortContent` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kno_FullContent` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kno_Tags` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kno_Galleries` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kno_VideoFile` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kno_View` int(11) NOT NULL DEFAULT 0,
  `Kno_Like` int(11) NOT NULL DEFAULT 0,
  `Kno_Status` int(11) NOT NULL DEFAULT 0 COMMENT '0 = off 1 = on',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`Kno_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`Kno_ID`, `Adm_ID`, `Gro_ID`, `Cat_ID`, `Sec_ID`, `Kno_Thumbnail`, `Kno_Title`, `Kno_ShortContent`, `Kno_FullContent`, `Kno_Tags`, `Kno_Galleries`, `Kno_VideoFile`, `Kno_View`, `Kno_Like`, `Kno_Status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, '240720_163845.jpg', 'knowledge1', 'test', '<p>tttttttt</p>', '[\"test\",\"test2\",\"test3\"]', '{\"img_gallery1\":\"310720_151606.jpg\",\"img_gallery2\":null,\"img_gallery3\":null,\"img_gallery4\":null,\"img_gallery5\":null,\"img_gallery6\":null,\"img_gallery7\":null,\"img_gallery8\":null,\"img_gallery9\":null,\"img_gallery10\":null}', '070820_153257.mp4', 31, 3, 1, '2020-07-24 16:38:45', '2020-08-11 11:12:42');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `Cat_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Gro_ID` int(11) NOT NULL DEFAULT 0,
  `Cat_Name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `Cat_Status` int(11) NOT NULL DEFAULT 0 COMMENT '0 = off 1 = on',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`Cat_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `Con_Detail` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`Con_Detail`, `created_at`, `updated_at`) VALUES
(NULL, '2019-09-08 00:00:00', '2019-10-20 16:14:20');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting` (
  `Set_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Set_Title` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Set_Logo` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Set_EmailAdmin` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Set_Description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Set_Keywords` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Set_Robots` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `Set_StatusWeb` int(11) DEFAULT NULL COMMENT '1 = on 0 = off',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`Set_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`Set_ID`, `Set_Title`, `Set_Logo`, `Set_EmailAdmin`, `Set_Description`, `Set_Keywords`, `Set_Robots`, `Set_StatusWeb`, `created_at`, `updated_at`) VALUES
(1, 'WE BLOG', '210820_190025.png', 'support@fameline.com', '', '', NULL, 0, '2019-08-11 00:00:00', '2020-08-21 19:00:25');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
CREATE TABLE IF NOT EXISTS `slider` (
  `Sli_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Sli_Name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `Sli_Img` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Sli_Status` int(11) NOT NULL COMMENT '0 = off 1 = on',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`Sli_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `Use_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Use_Username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Use_Password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Use_Fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Use_Email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Use_Tel` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Use_Department` int(11) DEFAULT 0,
  `Use_Section` int(11) NOT NULL DEFAULT 0 COMMENT 'only knowledge innovation',
  `Use_Permission` int(11) NOT NULL DEFAULT 1 COMMENT '0 = ยกเลิก 1 = ใช้งาน',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`Use_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`Use_ID`, `Use_Username`, `Use_Password`, `Use_Fullname`, `Use_Email`, `Use_Tel`, `Use_Department`, `Use_Section`, `Use_Permission`, `created_at`, `updated_at`) VALUES
(1, 'test', 'e10adc3949ba59abbe56e057f20f883e', 'test', 'abc@g.com', '1234567890', 1, 1, 1, '2020-06-09 11:33:04', '2020-07-30 16:22:37');

-- --------------------------------------------------------

--
-- Table structure for table `visit_web`
--

DROP TABLE IF EXISTS `visit_web`;
CREATE TABLE IF NOT EXISTS `visit_web` (
  `Viw_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Viw_IP` text COLLATE utf8_unicode_ci NOT NULL,
  `Viw_Browser` text COLLATE utf8_unicode_ci NOT NULL,
  `Viw_Device` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`Viw_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
