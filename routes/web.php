<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('page-offline', function () {
    return view('page_offline');
});

Route::group( [ 'middleware' => 'status_web'], function()
{
	//route user
	Route::get('/', 'Member\HomeController@index');

	Route::get('about', 'Member\AboutController@index');

	Route::get('category/{id}', 'Member\CategoriesController@main');

	Route::get('blog/all', 'Member\BlogController@detail');
	
	Route::get('blog/detail/{id?}', 'Member\BlogController@detail');

	Route::get('contact', 'Member\ContactController@index');
	//end route user

});




//route admin
Route::get('admin/login', 'Admin\LoginController@index');
Route::post('admin/login/auth', 'Admin\LoginController@auth');
Route::get('admin/logout', 'Admin\LoginController@logout');

//forgotpassword
Route::get('admin/forgotpassword', 'Admin\ForgotPasswordController@index');
Route::post('admin/forgotpassword/sendmail', 'Admin\ForgotPasswordController@sendmail');
Route::get('admin/forgotpassword/changepassword/{token_email}',
'Admin\ForgotPasswordController@changepassword');
Route::post('admin/forgotpassword/verify', 'Admin\ForgotPasswordController@verify');

Route::group( [ 'middleware' => 'session_login_admin'], function(){

	//home
	Route::get('admin/home', 'Admin\HomeController@index');

	//admin profile
	Route::get('admin/profile/{page}', 'Admin\ProfileController@page');
	Route::post('admin/profile/{page}/action', 'Admin\ProfileController@action');

	//admin setting
	Route::get('admin/setting/{page}', 'Admin\SettingController@page');
	Route::get('admin/setting/{page}/add', 'Admin\SettingController@form_add');
	Route::get('admin/setting/{page}/edit/{id}', 'Admin\SettingController@form_edit');
	Route::post('admin/setting/{page}/action', 'Admin\SettingController@action');
	Route::get('admin/setting/delete/{page}/{id}', 'Admin\SettingController@delete');

	//admin menu
	Route::get('admin/menu/{page}', 'Admin\MenuController@page');
	Route::post('admin/menu/{page}/action', 'Admin\MenuController@action');

	//admin users
	Route::get('admin/member/{page}', 'Admin\AdminController@page');
	Route::get('admin/member/{page}/add', 'Admin\AdminController@form_add');
	Route::get('admin/member/{page}/edit/{id}', 'Admin\AdminController@form_edit');
	Route::post('admin/member/{page}/action', 'Admin\AdminController@action');
	Route::get('admin/member/delete/{page}/{id}', 'Admin\AdminController@delete');

	//admin categories
	Route::get('admin/categories/{page}', 'Admin\CategoriesController@page');
	Route::get('admin/categories/{page}/add', 'Admin\CategoriesController@form_add');
	Route::get('admin/categories/{page}/edit/{id}', 'Admin\CategoriesController@form_edit');
	Route::post('admin/categories/{page}/action', 'Admin\CategoriesController@action');
	Route::get('admin/categories/delete/{page}/{id}', 'Admin\CategoriesController@delete');

	//admin knowledge
	Route::get('admin/blog/{page}', 'Admin\BlogController@page');
	Route::get('admin/blog/{page}/add', 'Admin\BlogController@form_add');
	Route::get('admin/blog/{page}/edit/{id}', 'Admin\BlogController@form_edit');
	Route::post('admin/blog/{page}/action', 'Admin\BlogController@action');
	Route::get('admin/blog/delete/{page}/{id}', 'Admin\BlogController@delete');

});
//end route admin


