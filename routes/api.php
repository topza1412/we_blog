<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//member
Route::post('course/review', 'Member\CourseController@review_course');
Route::get('addesss/getdata', 'Member\ApiController@getdata_address');

//admin
Route::get('admin/category/getdata', 'Admin\ApiController@getdata_category');
