<?php

return [

    'search_course' => 'Search course',
    'contact_admin' => 'Please contact admin',
    'add_success' => 'Successfully added data',
    'edit_success' => 'Successfully update data',
    'delete_success' => 'Successfully delete data',
    'add_not_success' => 'Failed to add data',
    'edit_not_success' => 'Failed to update data',
    'delete_not_success' => 'Failed to delete data',
    'data_not_found' => 'Data not found!',
];
