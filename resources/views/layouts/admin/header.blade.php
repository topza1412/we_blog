start: Header -->
<nav class="navbar navbar-default header navbar-fixed-top">
  <div class="col-md-12 nav-wrapper">
    <div class="navbar-header" style="width:100%;">
      <div class="opener-left-menu is-open">
        <span class="top"></span>
        <span class="middle"></span>
        <span class="bottom"></span>
      </div>
        <a href="{{url('admin/home')}}" class="navbar-brand"> 
         <b>{{SettingWeb::SettingWeb()->Set_Title}}</b>
        </a>

      <ul class="nav navbar-nav navbar-right user-nav">
        <li class="user-name"><span>@if(session('session_admin_id')){{session('session_admin_username')}}@endif</span></li>
          <li class="dropdown avatar-dropdown">
           <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><span class="icon-settings"></span></a>
           <ul class="dropdown-menu user-dropdown">
             <li><a href="{{url('admin/profile/profile')}}"><span class="fa fa-user"></span> My Profile</a></li>
             <li><a href="{{url('admin/profile/changepassword')}}"><span class="fa fa-expeditedssl"></span> Change password</a></li>
             <li><a href="{{url('admin/logout')}}"><span class="fa fa-power-off"></span> Logout</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
<!-- end: Header