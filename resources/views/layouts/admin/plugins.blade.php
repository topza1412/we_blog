<!-- sweet alert 2 -->
<script src="{{asset('assets/admin/plugins/sweetalert2/sweetalert2.all.min.js')}}"></script>

<!-- ckeditor -->
<script src="{{asset('assets/admin/plugins/ckeditor_4.6.2/ckeditor.js')}}"></script>

<!-- select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>


<script type="text/javascript">

//select2
$(".tag-select2").select2({
    tags: true,
    tokenSeparators: [',', ' ']
})

</script>

