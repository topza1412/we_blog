<!-- start:Left Menu -->
    <div id="left-menu">
      <div class="sub-left-menu scroll">
        <ul class="nav nav-list">
            <li><div class="left-bg"></div></li>
            <li class="time">
              <h1 class="animated fadeInLeft">{{date('H:i')}}</h1>
              <p class="animated fadeInRight">{{date('Y-m-d')}}</p>
            </li>
              <li class="ripple active"><a href="{{url('admin/home')}}"><span class="fa fa-dashboard"></span>Dashboard</a></li>
              @if(session('session_admin_status') == 1)
                <li class="ripple"><a class="tree-toggle nav-header"><span class="icons icon-note"></span> Setting  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                  <ul class="nav nav-list tree">
                    <li><a href="{{url('admin/setting/website')}}">Website</a></li>
                    <li><a href="{{url('admin/setting/seo')}}">Seo</a></li>
                    <li><a href="{{url('admin/setting/slider')}}">Slide</a></li>
                  </ul>
                </li>
                <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-clipboard"></span> Menu  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                  <ul class="nav nav-list tree">
                    <li><a href="{{url('admin/menu/about')}}">About</a></li>
                    <li><a href="{{url('admin/menu/contact')}}">Contact</a></li>
                  </ul>
                </li>
                <li class="ripple"><a href="{{url('admin/member/admin')}}"><span class="fa fa-user"></span> Members  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                </li>
              @endif
                <li class="ripple"><a href="{{url('admin/categories/main')}}"><span class="icons icon-support"></span> Categories  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                </li>
                <li class="ripple"><a href="{{url('admin/blog/main')}}"><span class="fa fa-book"></span> Blog  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                </li>
          </ul>
        </div>
    </div>
<!-- end: Left Menu -->


      <!-- start: Mobile -->
      <div id="mimin-mobile" class="reverse">
        <div class="mimin-mobile-menu-list">
            <div class="col-md-12 sub-mimin-mobile-menu-list animated fadeInLeft">
                <ul class="nav nav-list">
                  @if(session('session_admin_status') == 1)
                    <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-clipboard"></span> Setting  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                    <ul class="nav nav-list tree">
                      <li><a href="{{url('admin/setting/website')}}">Website</a></li>
                      <li><a href="{{url('admin/setting/seo')}}">Seo</a></li>
                      <li><a href="{{url('admin/setting/slider')}}">Slide</a></li>
                    </ul>
                  </li>
                  <li class="ripple"><a class="tree-toggle nav-header"><span class="fa fa-clipboard"></span> Menu  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                    <ul class="nav nav-list tree">
                      <li><a href="{{url('admin/menu/about')}}">About</a></li>
                      <li><a href="{{url('admin/menu/contact')}}">Contact</a></li>
                    </ul>
                  </li>
                    <li class="ripple"><a href="{{url('admin/member/admin')}}"><span class="fa fa-user"></span> Admin  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                    </li>
                  @endif
                    <li class="ripple"><a href="{{url('admin/categories/main')}}"><span class="fa fa-group"></span> Categories  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                    </li>
                    <li class="ripple"><a href="{{url('admin/blog/main')}}"><span class="fa fa-book"></span> Blog  <span class="fa-angle-right fa right-arrow text-right"></span> </a>
                    </li>
            </ul>
            </div>
        </div>       
      </div>
      <button id="mimin-mobile-menu-opener" class="animated rubberBand btn btn-circle btn-danger">
        <span class="fa fa-bars"></span>
      </button>
       <!-- end: Mobile -->