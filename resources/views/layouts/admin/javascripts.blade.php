<!-- js -->
<script src="{{asset('assets/admin/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/admin/js/jquery.ui.min.js')}}"></script>
<script src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/moment.min.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/jquery.datatables.min.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/datatables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/fullcalendar.min.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/jquery.vmap.min.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/maps/jquery.vmap.world.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/jquery.vmap.sampledata.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/chart.min.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/bootstrap-material-datetimepicker.js')}}"></script>
<script src="{{asset('assets/admin/js/main.js')}}"></script>

<script type="text/javascript">
//datables
$(document).ready(function(){
    $('#datatables-example').DataTable();
});

//date start
$('.datepickers').bootstrapMaterialDatePicker({ weekStart : 0, time: false,animation:true});
</script>


