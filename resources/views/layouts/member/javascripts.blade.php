<!-- js -->
<script src="{{asset('assets/member/js/jquery-1.12.4.min.js')}}"></script>
<script src="{{asset('assets/member/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('assets/member/js/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('assets/member/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/member/js/mmenu.min.js')}}"></script>
<script src="{{asset('assets/member/js/harvey.min.js')}}"></script>
<script src="{{asset('assets/member/js/waypoints.min.js')}}"></script>
<script src="{{asset('assets/member/js/facts.counter.min.js')}}"></script>
<script src="{{asset('assets/member/js/mixitup.min.js')}}"></script>
<script src="{{asset('assets/member/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/member/js/accordion.min.js')}}"></script>
<script src="{{asset('assets/member/js/responsive.tabs.min.js')}}"></script>
<script src="{{asset('assets/member/js/responsive.table.min.js')}}"></script>
<script src="{{asset('assets/member/js/masonry.min.js')}}"></script>
<script src="{{asset('assets/member/js/carousel.swipe.min.js')}}"></script>
<script src="{{asset('assets/member/js/bxslider.min.js')}}"></script>
<script src="{{asset('assets/member/js/main.js')}}"></script>

<!-- ห้ามคลิกขวา -->
<!-- <script type="text/javascript" language="javascript">
  $(function() {
     $(this).bind("contextmenu", function(e) {
        e.preventDefault();
     });
  });
</script> -->