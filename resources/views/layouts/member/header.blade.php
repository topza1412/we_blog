<!-- Start: Header Section -->
        <header id="header-v1" class="navbar-wrapper">
            <div class="container">
                <div class="row">
                    <nav class="navbar navbar-default">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="navbar-header">
                                    <div class="navbar-brand">
                                        <h1>
                                            <a href="{{url('/')}}">
                                                <img src="{{asset('upload/admin/logo_web/'.SettingWeb::SettingWeb()->Set_Logo)}}" alt="{{asset('upload/admin/logo_web/'.SettingWeb::SettingWeb()->Set_Title)}}" />
                                            </a>
                                        </h1>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <!-- Header Topbar -->
                                <div class="header-topbar hidden-sm hidden-xs">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="topbar-info">
                                                <a href="tel:+61-3-8376-6284"><i class="fa fa-phone"></i>+66(0)2365 5899</a>
                                                <span>/</span>
                                                <a href="mailto:support@fameline.com"><i class="fa fa-envelope"></i>info@fameline.com</a>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="topbar-links">
                                                @if(session('session_id') == null)
                                                    <a href="{{url('login')}}"><i class="fa fa-lock"></i>Login</a>
                                                @else
                                                    <a href="{{url('dashboard')}}"><i class="fa fa-user"></i>Dashboard</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="navbar-collapse hidden-sm hidden-xs">
                                    <ul class="nav navbar-nav">
                                        <li><a href="{{url('/')}}">Home</a></li>
                                        <!-- <li><a href="{{url('news')}}">News</a></li> -->
                                        <li><a href="{{url('contact')}}">Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="mobile-menu hidden-lg hidden-md">
                            <a href="#mobile-menu"><i class="fa fa-navicon"></i></a>
                            <div id="mobile-menu">
                                <ul>
                                    <li class="mobile-title">
                                        <h4>Navigation</h4>
                                        <a href="#" class="close"></a>
                                    </li>
                                    <li><a href="{{url('/')}}">Home</a></li>
                                    <!-- <li><a href="{{url('news')}}">News</a></li> -->
                                    <li><a href="{{url('contact')}}">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        <!-- End: Header Section -->

        <!-- Start: Slider Section -->
        <div data-ride="carousel" class="carousel slide" id="home-v1-header-carousel" style="opacity: 0.9;">
            
            <!-- Carousel slides -->
            <div class="carousel-inner">
                @foreach(SettingWeb::SlideBanner() as $key => $value)
                @if($key==0)
                  <?php $active = 'active';?>
                @else
                  <?php $active = null;?>
                @endif
                <div class="item {{$active}}">
                    <figure>
                        <img alt="{{$value['Sli_Name']}}" src="{{asset('upload/member/slide/'.$value['Sli_Img'])}}" />
                    </figure>
                </div>
                @endforeach
            </div>
            
            <!-- Controls -->
            <a class="left carousel-control" href="#home-v1-header-carousel" data-slide="prev"></a>
            <a class="right carousel-control" href="#home-v1-header-carousel" data-slide="next"></a>
        </div>
        <!-- End: Slider Section -->


        @if(session('session_id') != null)
            <!-- Start: Search Section -->
            <section class="search-filters">
                <div class="container">
                    <div class="filter-box">
                        <h3>Search data knowledge</h3>
                        <form action="{{url('knowledge/search')}}" method="post">
                            @csrf
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label class="sr-only" for="keywords">Keyword</label>
                                    <input class="form-control" placeholder="Search knowledge" style="font-size: 20px;" id="keywords" name="txt_search" type="text" required>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="form-group">
                                    <select name="group" id="group" class="form-control">
                                        <option value="">Categories</option>
                                        <option value="1">Core knowledge</option>
                                        <option value="2">Advanced knowledge</option>
                                        @if(session('session_department') != 1 && session('session_department') != 2)
                                            <option value="3">Innovation knowledge</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-6">
                                <div class="form-group">
                                    <input class="form-control" type="submit" value="Search">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
            <!-- End: Search Section -->
        @endif