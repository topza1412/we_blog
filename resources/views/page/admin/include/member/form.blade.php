<div class="card-body">
                <div class="basic-elements">
                    <form action="{{url('admin/member/'.$data['page'].'/action')}}" method="post" enctype="multipart/form-data" autocomplete="false">
                    @csrf

                    <input type="hidden" name="type_action" value="{{$data['action']}}">

                    <input type="hidden" name="id" value="@if(isset($data['id'])){{$data['id']}}@else{{''}}@endif">

                    @include('layouts.admin.flash-message')

                                  @if(count($errors))
                                      <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <div style="padding: 10px;">
                                          <ul>
                                              @foreach($errors->all() as $error)
                                                  <li>{{$error}}</li>
                                              @endforeach
                                          </ul>
                                        </div>
                                      </div>
                                 @endif

                        @if($data['page']=='admin')

                            @if(isset($data['detail']['Adm_Fullname']))
                                <?php $name = $data['detail']['Adm_Fullname'];?>
                            @else
                                <?php $name = null;?>
                            @endif

                            @if(isset($data['detail']['Adm_Username']))
                                <?php $username = $data['detail']['Adm_Username'];?>
                            @else
                                <?php $username = null;?>
                            @endif

                            @if(isset($data['detail']['Adm_Email']))
                                <?php $email = $data['detail']['Adm_Email'];?>
                            @else
                                <?php $email = null;?>
                            @endif

                            @if(isset($data['detail']['Adm_Tel']))
                                <?php $tel = $data['detail']['Adm_Tel'];?>
                            @else
                                <?php $tel = null;?>
                            @endif

                            @if(isset($data['detail']['Adm_Address']))
                                <?php $address = $data['detail']['Adm_Address'];?>
                            @else
                                <?php $address = null;?>
                            @endif

                            @if(isset($data['detail']['Adm_Department']))
                                <?php $department = $data['detail']['Adm_Department'];?>
                            @else
                                <?php $department = null;?>
                            @endif

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="name" type="text" class="form-text" required value="{{$name}}">
                                    <label>Full Name</label>
                                </div>
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="adm_username" type="text" class="form-text" required value="{{$username}}">
                                    <label>Username <span class="text-danger">*</span></label>
                                </div>
                                @if(!isset($data['detail']['Use_Password']))
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="password" id="show_gen_pass" type="text" class="form-text"  required>
                                    <label>Password (Password not less than 6 digits) <span class="text-danger">*</span></label>
                                    <button type="button" class="btn btn-warning btn-sm" onclick="gen_pass(6);">Generate password</button>
                                </div>
                                @endif
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="adm_email" type="text" class="form-text" required value="{{$email}}">
                                    <label>Email <span class="text-danger">*</span></label>
                                </div>
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="tel" type="text" class="form-text" required value="{{$tel}}">
                                    <label>Tel. <span class="text-danger">*</span></label>
                                </div>
                                <div class="form-group">
                                    <select name="department" class="form-control" required>
                                      <option value="">Department</option>
                                      <option value="1" @if($department == 1){{'selected'}}@endif>Marketing</option>
                                      <option value="2" @if($department == 2){{'selected'}}@endif>HR</option>
                                      <option value="3" @if($department == 3){{'selected'}}@endif>R&D</option>
                                      <option value="4" @if($department == 4){{'selected'}}@endif>Production</option>
                                    </select>
                                </div>
                                @if($data['action']=='update')
                                <div class="form-group">
                                    <input name="permission" type="radio" required @if($data['detail']['Use_Permission'] == 1){{'checked'}}@endif value="1"> Active
                                    <input name="permission" type="radio" required @if($data['detail']['Use_Permission'] == 0){{'checked'}}@endif value="0"> In-Active
                                </div>
                                @endif

                            </div>

                            <button type="submit" class="btn btn-info btn-flat m-b-30 m-t-30">Save</button>
                            &nbsp;
                            <button type="reset" class="btn btn-danger btn-flat m-b-30 m-t-30">Reset</button>

                        </div>
                        @endif


                        @if($data['page']=='user')

                            @if(isset($data['detail']['Use_Img']))
                                <?php $img = $data['detail']['Use_Img'];?>
                            @else
                                <?php $img = null;?>
                            @endif

                            @if(isset($data['detail']['Use_Fullname']))
                                <?php $name = $data['detail']['Use_Fullname'];?>
                            @else
                                <?php $name = null;?>
                            @endif

                            @if(isset($data['detail']['Use_Username']))
                                <?php $username = $data['detail']['Use_Username'];?>
                            @else
                                <?php $username = null;?>
                            @endif

                            @if(isset($data['detail']['Use_Email']))
                                <?php $email = $data['detail']['Use_Email'];?>
                            @else
                                <?php $email = null;?>
                            @endif

                            @if(isset($data['detail']['Use_Tel']))
                                <?php $tel = $data['detail']['Use_Tel'];?>
                            @else
                                <?php $tel = null;?>
                            @endif

                            @if(isset($data['detail']['Use_Line']))
                                <?php $line = $data['detail']['Use_Line'];?>
                            @else
                                <?php $line = null;?>
                            @endif

                            @if(isset($data['detail']['Use_Department']))
                                <?php $department = $data['detail']['Use_Department'];?>
                            @else
                                <?php $department = null;?>
                            @endif

                            @if(isset($data['detail']['Use_Section']))
                                <?php $section = $data['detail']['Use_Section'];?>
                            @else
                                <?php $section = null;?>
                            @endif


                        <div class="row">

                            <div class="col-lg-12">

                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="name" type="text" class="form-text" required value="{{$name}}">
                                    <label>Full Name</label>
                                </div>
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="use_username" type="text" class="form-text" required value="{{$username}}">
                                    <label>Username <span class="text-danger">*</span></label>
                                </div>
                                @if(!isset($data['detail']['Use_Password']))
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="password" id="show_gen_pass" type="text" class="form-text"  required>
                                    <label>Password (Password not less than 6 digits) <span class="text-danger">*</span></label>
                                    <button type="button" class="btn btn-warning btn-sm" onclick="gen_pass(6);">Generate password</button>
                                </div>
                                @endif
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="use_email" type="text" class="form-text" required value="{{$email}}">
                                    <label>Email <span class="text-danger">*</span></label>
                                </div>
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="tel" type="text" class="form-text" required value="{{$tel}}">
                                    <label>Tel. <span class="text-danger">*</span></label>
                                </div>
                                @if(session('session_admin_department') == 3 || session('session_admin_department') == 4 || session('session_admin_status') == 1)
                                    <div class="form-group">
                                        <label>Section Knowledge <span class="text-danger">(Only Knowledge innovation)</span></label>
                                        <select name="section" class="form-control" required>
                                          <option value="">Select</option>
                                          @foreach($data['section'] as $value)
                                            <option value="{{$value->Sec_ID}}" @if($section == $value->Sec_ID){{'selected'}}@endif>{{$value->Sec_Name}}</option>
                                          @endforeach
                                        </select>
                                    </div>
                                @endif
                                @if($data['action']=='update')
                                <div class="form-group">
                                    <input name="permission" type="radio" required @if($data['detail']['Use_Permission'] == 1){{'checked'}}@endif value="1"> Active
                                    <input name="permission" type="radio" required @if($data['detail']['Use_Permission'] == 0){{'checked'}}@endif value="0"> In-Active
                                </div>
                                @endif

                            </div>

                            <div class="col-lg-12">

                                <button type="submit" class="btn btn-info btn-flat m-b-30 m-t-30">Save</button>
                                &nbsp;
                                <button type="reset" class="btn btn-danger btn-flat m-b-30 m-t-30">Reset</button>

                            </div>

                        </div>
                        @endif


                        @if($data['page']=='role-admin')

                            @if(isset($data['detail']['Roa_All']))
                                <?php $role_all = $data['detail']['Roa_All'];?>
                            @else
                                <?php $role_all = null;?>
                            @endif

                            @if(isset($data['detail']['Roa_Member']))
                                <?php $role_member = $data['detail']['Roa_Member'];?>
                            @else
                                <?php $role_member = null;?>
                            @endif

                            @if(isset($data['detail']['Roa_KnowledgeAll']))
                                <?php $role_knowledge_all = $data['detail']['Roa_KnowledgeAll'];?>
                            @else
                                <?php $role_knowledge_all = null;?>
                            @endif

                            @if(isset($data['detail']['Roa_KnowledgeCore']))
                                <?php $role_knowledge_core = $data['detail']['Roa_KnowledgeCore'];?>
                            @else
                                <?php $role_knowledge_core = null;?>
                            @endif

                            @if(isset($data['detail']['Roa_KnowledgeAdvance']))
                                <?php $role_knowledge_advance = $data['detail']['Roa_KnowledgeAdvance'];?>
                            @else
                                <?php $role_knowledge_advance = null;?>
                            @endif

                            @if(isset($data['detail']['Roa_KnowledgeInnovation']))
                                <?php $role_knowledge_innovation = $data['detail']['Roa_KnowledgeInnovation'];?>
                            @else
                                <?php $role_knowledge_innovation = null;?>
                            @endif

                            @if(isset($data['detail']['Roa_CategoryAll']))
                                <?php $role_category_all = $data['detail']['Roa_CategoryAll'];?>
                            @else
                                <?php $role_category_all = null;?>
                            @endif

                            @if(isset($data['detail']['Roa_CategoryCore']))
                                <?php $role_category_core = $data['detail']['Roa_CategoryCore'];?>
                            @else
                                <?php $role_category_core = null;?>
                            @endif

                            @if(isset($data['detail']['Roa_CategoryAdvance']))
                                <?php $role_category_advance = $data['detail']['Roa_CategoryAdvance'];?>
                            @else
                                <?php $role_category_advance = null;?>
                            @endif

                            @if(isset($data['detail']['Roa_CategoryInnovation']))
                                <?php $role_category_innovation = $data['detail']['Roa_CategoryInnovation'];?>
                            @else
                                <?php $role_category_innovation = null;?>
                            @endif

                            @if(isset($data['detail']['Roa_Section']))
                                <?php $role_section = $data['detail']['Roa_Section'];?>
                            @else
                                <?php $role_section = null;?>
                            @endif

                            @if(isset($data['detail']['Roa_ReportsAll']))
                                <?php $role_reports_all = $data['detail']['Roa_ReportsAll'];?>
                            @else
                                <?php $role_reports_all = null;?>
                            @endif

                            @if(isset($data['detail']['Roa_ReportsLogKnowledge']))
                                <?php $role_reports_log_knowledge = $data['detail']['Roa_ReportsLogKnowledge'];?>
                            @else
                                <?php $role_reports_log_knowledge = null;?>
                            @endif

                            @if(isset($data['detail']['Roa_ReportsLogSystem']))
                                <?php $role_reports_log_system = $data['detail']['Roa_ReportsLogSystem'];?>
                            @else
                                <?php $role_reports_log_system = null;?>
                            @endif

                            @if(isset($data['detail']['Roa_ReportsMember']))
                                <?php $role_reports_member = $data['detail']['Roa_ReportsMember'];?>
                            @else
                                <?php $role_reports_member = null;?>
                            @endif

                            @if(isset($data['detail']['Roa_ReportsVisit']))
                                <?php $role_reports_visit = $data['detail']['Roa_ReportsVisit'];?>
                            @else
                                <?php $role_reports_visit = null;?>
                            @endif


                        <div class="row">

                            <div class="col-lg-6">

                                <div class="form-group form-animate-checkbox">
                                    <input name="role_all" type="checkbox" id="section_all" class="form-text" @if($role_all != null){{'checked'}}@endif value="1">
                                    <label><h4>All Menu</h4></label>
                                </div>

                                <div class="form-group form-animate-checkbox">
                                    <input name="role_member" type="checkbox" class="form-text section_all" @if($role_member != null){{'checked'}}@endif value="1">
                                    <label><h4>Member</h4></label>
                                </div>

                                <div class="form-group form-animate-checkbox">
                                    <input name="role_knowledge_all" type="checkbox" id="knowledge_all" class="form-text section_all" @if($role_knowledge_all != null){{'checked'}}@endif value="1">
                                    <label><h4>Knowledge All</h4></label>
                                </div>

                                <div class="form-group form-animate-checkbox">
                                    <input name="role_knowledge_core" type="checkbox" class="form-text section_all knowledge_all" @if($role_knowledge_core != null){{'checked'}}@endif value="1">
                                    <label><h4>Knowledge Core</h4></label>
                                </div>

                                <div class="form-group form-animate-checkbox">
                                    <input name="role_knowledge_advance" type="checkbox" class="form-text section_all knowledge_all" @if($role_knowledge_advance != null){{'checked'}}@endif value="1">
                                    <label><h4>Knowledge Advance</h4></label>
                                </div>

                                <div class="form-group form-animate-checkbox">
                                        <input name="role_knowledge_innovation" type="checkbox" class="form-text section_all knowledge_all" @if($role_knowledge_innovation != null){{'checked'}}@endif value="1">
                                        <label><h4>Knowledge Innovation</h4></label>
                                </div>

                                <div class="form-group form-animate-checkbox">
                                    <input name="role_category_all" type="checkbox" id="category_all" class="form-text section_all" @if($role_category_all != null){{'checked'}}@endif value="1">
                                    <label><h4>Category All</h4></label>
                                </div>

                                <div class="form-group form-animate-checkbox">
                                    <input name="role_category_core" type="checkbox" class="form-text section_all category_all" @if($role_category_core != null){{'checked'}}@endif value="1">
                                    <label><h4>Category Core</h4></label>
                                </div>

                                <div class="form-group form-animate-checkbox">
                                    <input name="role_category_advance" type="checkbox" class="form-text section_all category_all" @if($role_category_advance != null){{'checked'}}@endif value="1">
                                    <label><h4>Category Advance</h4></label>
                                </div>

                                <div class="form-group form-animate-checkbox">
                                    <input name="role_category_innovation" type="checkbox" class="form-text section_all category_all" @if($role_category_innovation != null){{'checked'}}@endif value="1">
                                    <label><h4>Category Innovation</h4></label>
                                </div>

                                <div class="form-group form-animate-checkbox">
                                    <input name="role_reports_all" type="checkbox" id="reports_all" class="form-text section_all" @if($role_reports_all != null){{'checked'}}@endif value="1">
                                    <label><h4>Reports All</h4></label>
                                </div>

                                <div class="form-group form-animate-checkbox">
                                    <input name="role_reports_log_knowledge" type="checkbox" class="form-text section_all reports_all" @if($role_reports_log_knowledge != null){{'checked'}}@endif value="1">
                                    <label><h4>Reports Logs knowledge</h4></label>
                                </div>

                                <div class="form-group form-animate-checkbox">
                                    <input name="role_reports_log_system" type="checkbox" class="form-text section_all reports_all" @if($role_reports_log_system != null){{'checked'}}@endif value="1">
                                    <label><h4>Reports Logs system</h4></label>
                                </div>

                                <div class="form-group form-animate-checkbox">
                                    <input name="role_reports_member" type="checkbox" class="form-text section_all reports_all" @if($role_reports_member != null){{'checked'}}@endif value="1">
                                    <label><h4>Reports Member</h4></label>
                                </div>

                                <div class="form-group form-animate-checkbox">
                                    <input name="role_reports_visit" type="checkbox" class="form-text section_all reports_all" @if($role_reports_visit != null){{'checked'}}@endif value="1">
                                    <label><h4>Reports Visit</h4></label>
                                </div>

                            </div>

                            <div class="col-lg-12">

                                <button type="submit" class="btn btn-info btn-flat m-b-30 m-t-30">Save</button>
                                &nbsp;
                                <button type="reset" class="btn btn-danger btn-flat m-b-30 m-t-30">Reset</button>

                            </div>

                        </div>
                        @endif


                    </form>

                </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">
function gen_pass(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   
   $("#show_gen_pass").val(result);
}

$(function() {
  $("#section_all").click(disabled_section_all);
  $("#knowledge_all").click(disabled_knowledge_all);
  $("#category_all").click(disabled_category_all);
  $("#reports_all").click(disabled_reports_all);
});


function disabled_section_all() {
  if (this.checked) {
    $("input.section_all").prop("disabled", $(this).is(":checked"));
    $("input.section_all").removeAttr('checked');
  } else {
    $("input.section_all").prop("disabled", $(this).is(":checked"));
  }
}
function disabled_knowledge_all() {
  if (this.checked) {
    $("input.knowledge_all").prop("disabled", $(this).is(":checked"));
    $("input.knowledge_all").removeAttr('checked');
  } else {
    $("input.knowledge_all").prop("disabled", $(this).is(":checked"));
  }
}
function disabled_category_all() {
  if (this.checked) {
    $("input.category_all").prop("disabled", $(this).is(":checked"));
    $("input.category_all").removeAttr('checked');
  } else {
    $("input.category_all").prop("disabled", $(this).is(":checked"));
  }
}
function disabled_reports_all() {
  if (this.checked) {
    $("input.reports_all").prop("disabled", $(this).is(":checked"));
    $("input.reports_all").removeAttr('checked');
  } else {
    $("input.reports_all").prop("disabled", $(this).is(":checked"));
  }
}
</script>