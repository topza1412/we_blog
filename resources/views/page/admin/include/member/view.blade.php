<div class="responsive-table">

      <a href="{{url('admin/member/admin/add')}}" class="btn btn-warning"><i class="fa fa-edit f-s-15"></i> New+</a>
      <br><br>
      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
      <thead>
        <tr>
        <th>#</th>
        <th>Full Name</th>
        <th>Username</th>
        <th>Email</th>
        <th>Tel</th>
        <th>Permission</th>
        <th>Date create</th>
        <th>Manage</th>
        </tr>
      </thead>
      <tbody>
        @if(count($data['detail'])>0)
            <?php $i = 1;?>
            @foreach($data['detail'] as $value)
            <tr>
            <td>{{$i++}}</td>
            <td>{{$value->Adm_Fullname}}</td>
            <td>{{$value->Adm_Username}}</td>
            <td>{{$value->Adm_Email}}</td>
            <td>{{$value->Adm_Tel}}</td>
            <td>
            @if($value->Adm_Permission==1)
              <span class="badge badge-success">Active</span>
            @else
              <span class="badge badge-danger">In-Active</span>
            @endif
           </td>
            <td>{{date("d-m-Y",strtotime($value->created_at))}}</td>
            <td>
            <div class="btn-group" role="group">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="fa fa-pencil-square-o"></span> Action
              <span class="fa fa-angle-down"></span>
            </button>
            <ul class="dropdown-menu">
              <li><a href="{{url('admin/member/admin/edit/'.$value->Adm_ID)}}" class="dropdown-item">Edit</a></li>
              <li><a href="#" class="dropdown-item" onclick="confirm_delete('{{url('admin/member/delete/admin/')}}','{{$value->Adm_ID}}');">Delete</a></li>
            </ul>
           </div>
            </td>
            </tr>
            @endforeach
        @else
            <tr>
            <td colspan="8" class="text-danger"><div align="center">No data not found!</div></td>    
            </tr>
        @endif
       </tbody>
       </table>

</div>

