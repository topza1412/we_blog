<div class="responsive-table">

@if($data['page'] == 'core')
<?php $group = 'Core knowledge';?>
@elseif($data['page'] == 'advance')
<?php $group = 'Advance knowledge';?>
@elseif($data['page'] == 'innovation')
<?php $group = 'Innovation knowledge';?>
@endif

      <a href="{{url('admin/knowledge/'.$data['page'].'/add')}}" class="btn btn-warning"><i class="fa fa-edit f-s-15"></i> New+</a>
      <br><br>
      <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
      <thead>
        <tr class="small">
        <th>#</th>
        <th>Thumbnail</th>
        <th>Title</th>
        <th>Group</th>
        <th>Category</th>
        <th>Description</th>
        <th>Create by</th>
        <th>View</th>
        <th>Date create</th>
        <th>Status</th>
        <th>Manage</th>
        </tr>
      </thead>
      <tbody>
        @if(count($data['result'])>0)
            <?php $i = 1;?>
            @foreach($data['result'] as $value)
              @if($value->Gro_ID == 1)
                <?php $group = 'Core knowledge';?>
              @elseif($value->Gro_ID == 2)
                <?php $group = 'Advance knowledge';?>
              @elseif($value->Gro_ID == 3)
                <?php $group = 'Innovation knowledge';?>
              @endif

            <tr class="small">
            <td>{{$i++}}</td>
            <td align="center">
            @if($value->Kno_Thumbnail!=null)
            <a target="_blank" href="{{asset('upload/member/knowledge/thumbnail/'.$data['page'].'/'.$value->Kno_Thumbnail)}}">
              <img src="{{asset('upload/member/knowledge/thumbnail/'.$data['page'].'/'.$value->Kno_Thumbnail)}}" width="100">
            </a>
            @else
              <img src="{{asset('upload/member/no-image.jpg')}}" width="100">
            @endif
            </td>
            <td>{{$value->Kno_Title}}</td>
            <td>{{$group}}</td>
            <td>{{$value->Cat_Name}}</td>
            <td>{{$value->Kno_ShortContent}}</td>
            <td>{{$value->Adm_Username}}</td>
            <td>{{$value->Kno_View}}</td>
            <td>{{date("d-m-Y",strtotime($value->created_at))}}</td>
            <td>
            @if($value->Kno_Status==1)
            <span class="badge badge-success">Active</span>
            @else
            <span class="badge badge-danger">In-Active</span>
            @endif</td>
            <td>
            <div class="btn-group" role="group">
            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="fa fa-pencil-square-o"></span> Action
              <span class="fa fa-angle-down"></span>
            </button>
            <ul class="dropdown-menu">
              <li><a href="{{url('knowledge/detail/'.$value->Kno_ID)}}" class="dropdown-item" target="_blank">View</a></li>
              <li><a href="{{url('admin/knowledge/report/'.$value->Kno_ID)}}" class="dropdown-item" target="_blank">Report</a></li>
              <li><a href="{{url('admin/knowledge/'.$data['page'].'/edit/'.$value->Kno_ID)}}" class="dropdown-item">Edit</a></li>
              <li><a href="#" class="dropdown-item" onclick="confirm_delete('{{url('admin/knowledge/delete/knowledge')}}','{{$value->Kno_ID}}');">Delete</a></li>
            </ul>
           </div>
            </td>
            </tr>
            @endforeach
        @else
            <tr>
            <td colspan="11" class="text-danger"><div align="center">No data not found!</div></td>    
            </tr>
        @endif
       </tbody>
       </table>

</div>

