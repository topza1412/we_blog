<div class="card-body">
                <div class="basic-elements">
                    <form action="{{url('admin/knowledge/'.$data['page'].'/action')}}" method="post" enctype="multipart/form-data" autocomplete="false">
                    @csrf

                    <input type="hidden" name="type_action" value="{{$data['action']}}">

                    <input type="hidden" name="id" value="@if(isset($data['id'])){{$data['id']}}@else{{''}}@endif">

                    @include('layouts.admin.flash-message')

                                  @if(count($errors))
                                      <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <div style="padding: 10px;">
                                          <ul>
                                              @foreach($errors->all() as $error)
                                                  <li>{{$error}}</li>
                                              @endforeach
                                          </ul>
                                        </div>
                                      </div>
                                 @endif

                            @if(isset($data['result']['Kno_Thumbnail']))
                                <?php $thumbnail = $data['result']['Kno_Thumbnail'];?>
                            @else
                                <?php $thumbnail = null;?>
                            @endif   

                            @if(isset($data['result']['Kno_Title']))
                                <?php $title = $data['result']['Kno_Title'];?>
                            @else
                                <?php $title = null;?>
                            @endif                            

                            @if(isset($data['result']['Gro_ID']))
                                <?php $group = $data['result']['Gro_ID'];?>
                              @if($data['result']['Gro_ID'] == 1)
                                <?php $group_name = 'core';?>
                              @elseif ($data['result']['Gro_ID'] == 2)
                                <?php $group_name = 'advance';?>
                              @elseif ($data['result']['Gro_ID'] == 3)
                                <?php $group_name = 'innovation';?>
                              @endif
                            @else
                                <?php $group = null;?>
                            @endif

                            @if(isset($data['result']['Cat_ID']))
                                <?php $category = $data['result']['Cat_ID'];?>
                            @else
                                <?php $category = null;?>
                            @endif

                            @if(isset($data['result']['Sec_ID']))
                                <?php $section = $data['result']['Sec_ID'];?>
                            @else
                                <?php $section = null;?>
                            @endif

                            @if(isset($data['result']['Kno_ShortContent']))
                                <?php $shortcontent = $data['result']['Kno_ShortContent'];?>
                            @else
                                <?php $shortcontent = null;?>
                            @endif    

                            @if(isset($data['result']['Kno_FullContent']))
                                <?php $fullcontent = $data['result']['Kno_FullContent'];?>
                            @else
                                <?php $fullcontent = null;?>
                            @endif    

                            @if(isset($data['result']['Kno_Tags']))
                                <?php $tags = json_decode($data['result']['Kno_Tags']);?>
                            @else
                                <?php $tags = null;?>
                            @endif  

                            @if(isset($data['result']['Kno_Galleries']))
                                <?php $galleries = $data['result']['Kno_Galleries'];?>
                            @else
                                <?php $galleries = null;?>
                            @endif   

                            @if(isset($data['result']['Kno_VideoFile']))
                                <?php $video_file = $data['result']['Kno_VideoFile'];?>
                            @else
                                <?php $video_file = null;?>
                            @endif      

                        <div class="row">
                            <div class="col-lg-12">
                              <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="title" type="text" class="form-text" required value="{{$title}}">
                                    <label>Title <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                  @if($thumbnail != null)
                                    <input type="hidden" name="thumbnail_hidden" value="{{$thumbnail}}">
                                    <a target="_blank" href="{{asset('upload/member/knowledge/thumbnail/'.$data['page'].'/'.$thumbnail)}}">
                                      <img src="{{asset('upload/member/knowledge/thumbnail/'.$data['page'].'/'.$thumbnail)}}" width="270" title="view">
                                    </a>
                                  @endif
                                    <input name="thumbnail" type="file" class="form-text" placeholder="Thumbnail">
                                    <p class="small text-danger">*Image size not over 2 mb. 960*650 || Support only file jpeg,jpg,png</p>
                                    <label>Thumbnail</label>
                                </div>
                                <div class="form-group">
                                    <label>Group <span class="text-danger">*</span></label>
                                    <select name="group" class="form-control" required>
                                      <option value="">Select</option>
                                      @if(session('session_admin_status') != 1)
                                        @if(session('session_admin_role')->Roa_All || session('session_admin_role')->Roa_KnowledgeAll)
                                          @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_KnowledgeCore)
                                            <option value="1" @if($group == 1){{'selected'}}@endif>Core knowledge</option>
                                          @endif
                                          @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_KnowledgeAdvance)
                                            <option value="2" @if($group == 2){{'selected'}}@endif>Advance knowledge</option>
                                          @endif
                                          @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_KnowledgeInnovation)
                                            <option value="3" @if($group == 3){{'selected'}}@endif>Innovation knowledge</option>
                                          @endif
                                        @endif
                                      @else
                                        <option value="1" @if($group == 1){{'selected'}}@endif>Core knowledge</option>
                                        <option value="2" @if($group == 2){{'selected'}}@endif>Advance knowledge</option>
                                        <option value="3" @if($group == 3){{'selected'}}@endif>Innovation knowledge</option>
                                      @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Categories <span class="text-danger">*</span></label>
                                    <select name="categories" class="form-control" required>
                                      <option value="">Select</option>
                                      @foreach($data['categories'] as $value)
                                        <option value="{{$value->Cat_ID}}" @if($category == $value->Cat_ID){{'selected'}}@endif>{{$value->Cat_Name}}</option>
                                      @endforeach
                                    </select>
                                </div>
                                @if(session('session_admin_status') != 1)
                                  @if(session('session_admin_department') == 3 || session('session_admin_department') == 4)
                                    <div class="form-group">
                                        <label>Section Knowledge innovation</label>
                                        <select name="section" class="form-control" required>
                                            <option value="">Select</option>
                                            @foreach($data['section'] as $value)
                                              <option value="{{$value->Sec_ID}}" @if($section == $value->Sec_ID){{'selected'}}@endif>{{$value->Sec_Name}}</option>
                                            @endforeach
                                          </select>
                                    </div>
                                  @endif
                                @else
                                  <div class="form-group">
                                        <label>Section Knowledge innovation</label>
                                        <select name="section" class="form-control" required>
                                            <option value="">Select</option>
                                            @foreach($data['section'] as $value)
                                              <option value="{{$value->Sec_ID}}" @if($section == $value->Sec_ID){{'selected'}}@endif>{{$value->Sec_Name}}</option>
                                            @endforeach
                                          </select>
                                    </div>
                                @endif
                                <div class="form-group">
                                  <label>Short Content <span class="text-danger">*</span></label>
                                  <textarea name="short_content" class="form-control" required>{{$shortcontent}}</textarea>
                                </div>
                                <div class="form-group">
                                  <label>Tags<span class="text-danger">*</span></label>
                                  <select name="tags[]" class="tag-select2 form-control" multiple="multiple">
                                    @if(isset($tags))
                                      @foreach($tags as $value)
                                        <option value="{{$value}}" selected="selected">{{$value}}</option>
                                      @endforeach
                                    @endif
                                  </select>
                                </div>

                            </div>

                            <div class="col-lg-8">
                                <div class="form-group">
                                  <label>Full Content <span class="text-danger">*</span></label>
                                  <textarea name="full_content" class="ckeditor" >{{$fullcontent}}</textarea>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-lg-5">
                              <b>Galleries images</b>
                              <hr>
                              @if($galleries == null)
                                @for($i=1;$i<=10;$i++)
                                    <div class="form-group">
                                        <label>Image gallery{{$i}}</label>
                                        <input name="img_gallery{{$i}}" type="file" class="form-text" placeholder="Image">
                                        <p class="small text-danger">*Image size not over 2 mb. 960*550 || Support only file jpeg,jpg,png</p>
                                    </div>
                                @endfor
                              @else
                                <?php $n = 1;?>
                                @foreach(json_decode($galleries) as $key => $value)
                                    <div class="form-group">
                                        <label>Image gallery{{$n}}</label>
                                        <br>
                                      @if($value != null)
                                        <input type="hidden" name="{{$key}}_hidden" value="{{$value}}">
                                        <a target="_blank" href="{{asset('upload/member/knowledge/galleries/'.$group_name.'/'.$value)}}">
                                          <img src="{{asset('upload/member/knowledge/galleries/'.$group_name.'/'.$value)}}" width="210">
                                        </a>
                                      @endif
                                        <input name="img_gallery{{$n}}" type="file" class="form-text" placeholder="Image">
                                        <p class="small text-danger">*Image size not over 2 mb. 960*550 || Support only file jpeg,jpg,png</p>
                                    </div>
                                    <?php $n++;?>
                                  @endforeach
                              @endif
                            </div>


                            <div class="col-lg-7">
                              <b>Video media</b>
                              <hr>
                              <div class="form-group">
                                  @if($video_file != null)
                                    <input type="hidden" name="video_file_hidden" value="{{$video_file}}">
                                    <video poster="{{asset('upload/member/knowledge/thumbnail/'.$group_name.'/'.$thumbnail)}}" id="player" playsinline controls width="250">
                                       <source src="{{asset('upload/member/knowledge/video/'.$group_name.'/'.$video_file)}}" type="video/mp4">
                                       <source src="{{asset('upload/member/knowledge/video/'.$group_name.'/'.$video_file)}}" type="video/webm">
                                       <track kind="captions" label="Video preview" src="{{asset('upload/member/knowledge/video/'.$group_name.'/'.$video_file)}}" srclang="en" default>
                                    </video>
                                  @endif
                                    <label>Video file</label>
                                    <input name="video_file" type="file" class="form-text" placeholder="Video file">
                                    <p class="small text-danger">*Video size not over 50 mb. || Support only file mp4,mpeg</p>
                                </div>
                                @if($data['action']=='update')
                                <div class="form-group">
                                    <label>Public content</label>
                                    <br>
                                    <input name="status" type="radio" required @if($data['result']['Kno_Status'] == 1){{'checked'}}@endif value="1"> Active
                                    <input name="status" type="radio" required @if($data['result']['Kno_Status'] == 0){{'checked'}}@endif value="0"> In-Active
                                </div>
                                @endif
                            </div>

                            <div class="col-lg-12">

                                <button type="submit" class="btn btn-info btn-flat m-b-30 m-t-30">Save</button>
                                &nbsp;
                                <button type="reset" class="btn btn-danger btn-flat m-b-30 m-t-30">Reset</button>
                            </div>

                        </div>

                    </form>

                </div>
            </div>
