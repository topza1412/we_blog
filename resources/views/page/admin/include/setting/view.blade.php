<form action="{{url('admin/setting/'.$data['page'].'/action')}}" method="post" enctype="multipart/form-data">
@csrf

    @include('layouts.admin.flash-message')

      @if(count($errors))
          <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <div style="padding: 10px;">
              <ul>
                  @foreach($errors->all() as $error)
                      <li>{{$error}}</li>
                  @endforeach
              </ul>
            </div>
          </div>
     @endif

    @if($data['page']=='website')
        @foreach($data['result'] as $value)
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group form-animate-text" style="margin-top:40px !important;">
                        <input name="title_web" type="text" class="form-text" required value="{{$value->Set_Title}}">
                        <label>Name website</label>
                    </div>
                    <div class="form-group form-animate-text" style="margin-top:40px !important;">
                        @if($value->Set_Logo!=null)
                        <input type="hidden" name="logo_web_hidden" value="{{$value->Set_Logo}}">
                        <img src="{{asset('upload/admin/logo_web/'.$value->Set_Logo)}}" width="180">
                        @endif
                        <input name="logo_web" type="file" class="form-text" placeholder="Logo Web">
                        <p class="small text-danger">*Image size not over 2 mb. 190*45 || Support only file jpeg,jpg,png</p>
                        <label>Logo web</label>
                    </div>
                    <div class="form-group form-animate-text" style="margin-top:40px !important;">
                        <input name="email_admin" type="text" class="form-text" value="{{$value->Set_EmailAdmin}}">
                        <label>Email website</label>
                    </div>

                    <div class="form-group">
                        <label>Status website</label>
                        <br>
                        <input name="status_web" type="radio" required @if($value->Set_StatusWeb==1){{'checked'}}@endif value="1"> <span style="color:green;"><b>Online</b></span>
                        <input name="status_web" type="radio" required @if($value->Set_StatusWeb==0){{'checked'}}@endif value="0"> <span style="color:red;"><b>Offline</b></span>
                    </div>

                </div>

                <div class="col-lg-12">
                    <button type="submit" class="btn btn-info btn-flat m-b-30 m-t-30">Save</button>
                    &nbsp;
                    <button type="reset" class="btn btn-danger btn-flat m-b-30 m-t-30">Reset</button>
                </div>

            </div>
        @endforeach
    @endif

    @if($data['page']=='seo')
        @foreach($data['result'] as $value)
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label>Description seo website <span class="text-danger">(Example we blog, blog)</span></label>
                        <textarea name="description_seo" class="form-control" >{{$value->Set_Description}}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Keyword seo website <span class="text-danger">(Example we blog, blog)</span></label>
                        <textarea name="keyword_seo" class="form-control" >{{$value->Set_Keywords}}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Keyword bot seo website <span class="text-danger">(Example we blog, blog)</span></label>
                        <textarea name="robot_seo" class="form-control" >{{$value->Set_Robots}}</textarea>
                    </div>

                </div>
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-info btn-flat m-b-30 m-t-30">Save</button>
                    &nbsp;
                    <button type="reset" class="btn btn-danger btn-flat m-b-30 m-t-30">Reset</button>
                </div>
            </div>
        @endforeach
    @endif

    </form>

    @if($data['page']=='slider')
        <div class="row">
            <div class="col-lg-12">
            <div class="responsive-table">
              <a href="{{url('admin/setting/slider/add')}}" class="btn btn-warning"><i class="fa fa-edit f-s-15"></i> New+</a>
              <br><br>
              <table id="datatables-example" class="table table-striped table-bordered" width="100%" cellspacing="0">
              <thead>
                <tr>
                <th>#</th>
                <th>Image slide</th>
                <th>Name image slide</th>
                <th>Date create</th>
                <th>Date update</th>
                <th>Manage</th>
                </tr>
              </thead>
              <tbody>
                @if(count($data['result'])>0)
                    <?php $i = 1;?>
                    @foreach($data['result'] as $value)
                    <tr>
                    <td>{{$i++}}</td>
                    <td><a href="{{asset('upload/member/slide/'.$value->Sli_Img)}}" target="_blank"><img src="{{asset('upload/member/slide/'.$value->Sli_Img)}}" width="160px"></a></td>
                    <td>{{$value->Sli_Name}}</td>
                    <td>{{date("d-m-Y",strtotime($value->created_at))}}</td>
                    <td>{{date("d-m-Y",strtotime($value->updated_at))}}</td>
                    <td>
                    <div class="btn-group" role="group">
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="fa fa-pencil-square-o"></span> Action
                      <span class="fa fa-angle-down"></span>
                    </button>
                    <ul class="dropdown-menu">
                      <li><a href="{{url('admin/setting/slider/edit/'.$value->Sli_ID)}}" class="btn btn-success">Edit</a></li>
                      <li><a href="#" class="btn btn-danger" onclick="confirm_delete('{{url('admin/setting/delete/slider/')}}','{{$value->Sli_ID}}');">Delete</a></li>
                    </ul>
                   </div>
                    </td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                    <td colspan="5" class="text-danger"><div align="center">No data not found!</div></td>    
                    </tr>
                @endif
               </tbody>
               </table>
            </div>  

@endif
