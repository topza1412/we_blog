<div class="card-body">
                <div class="basic-elements">
                    <form action="{{url('admin/categories/'.$data['page'].'/action')}}" method="post" enctype="multipart/form-data" autocomplete="false">
                    @csrf

                    <input type="hidden" name="type_action" value="{{$data['action']}}">

                    <input type="hidden" name="id" value="@if(isset($data['id'])){{$data['id']}}@else{{''}}@endif">

                    @include('layouts.admin.flash-message')

                                  @if(count($errors))
                                      <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <div style="padding: 10px;">
                                          <ul>
                                              @foreach($errors->all() as $error)
                                                  <li>{{$error}}</li>
                                              @endforeach
                                          </ul>
                                        </div>
                                      </div>
                                 @endif

                            @if(isset($data['result']['Cat_Name']))
                                <?php $name = $data['result']['Cat_Name'];?>
                            @else
                                <?php $name = null;?>
                            @endif

                            @if(isset($data['result']['Gro_ID']))
                                <?php $group = $data['result']['Gro_ID'];?>
                            @else
                                <?php $group = null;?>
                            @endif

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="name" type="text" class="form-text" required value="{{$name}}">
                                    <label>Category name</label>
                                </div>
                                <div class="form-group">
                                    <label>Group <span class="text-danger">*</span></label>
                                    <select name="group" class="form-control" required>
                                      <option value="">Select</option>
                                      @if(session('session_admin_status') != 1)
                                        @if(session('session_admin_role')->Roa_All || session('session_admin_role')->Roa_KnowledgeAll)
                                          @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_KnowledgeCore)
                                            <option value="1" @if($group == 1){{'selected'}}@endif>Core knowledge</option>
                                          @endif
                                          @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_KnowledgeAdvance)
                                            <option value="2" @if($group == 2){{'selected'}}@endif>Advance knowledge</option>
                                          @endif
                                          @if(session('session_admin_role')->Roa_KnowledgeAll || session('session_admin_role')->Roa_KnowledgeInnovation)
                                            <option value="3" @if($group == 3){{'selected'}}@endif>Innovation knowledge</option>
                                          @endif
                                        @endif
                                      @else
                                        <option value="1" @if($group == 1){{'selected'}}@endif>Core knowledge</option>
                                        <option value="2" @if($group == 2){{'selected'}}@endif>Advance knowledge</option>
                                        <option value="3" @if($group == 3){{'selected'}}@endif>Innovation knowledge</option>
                                      @endif
                                    </select>
                                </div>
                                @if($data['action']=='update')
                                <div class="form-group">
                                    <input name="permission" type="radio" required @if($data['result']['Cat_Status'] == 1){{'checked'}}@endif value="1"> Active
                                    <input name="permission" type="radio" required @if($data['result']['Cat_Status'] == 0){{'checked'}}@endif value="0"> In-Active
                                </div>
                                @endif

                            </div>

                            <div class="col-lg-12">

                                <button type="submit" class="btn btn-info btn-flat m-b-30 m-t-30">Save</button>
                                &nbsp;
                                <button type="reset" class="btn btn-danger btn-flat m-b-30 m-t-30">Reset</button>
                            </div>

                        </div>

                    </form>

                </div>
            </div>