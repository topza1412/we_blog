<div class="card-body">
                <div class="basic-elements">
                    <form action="{{url('admin/profile/'.$data['page'].'/action')}}" method="post" enctype="multipart/form-data" autocomplete="false">
                    @csrf

                    <input type="hidden" name="type_action" value="{{$data['action']}}">

                    <input type="hidden" name="id" value="@if(isset($data['id'])){{$data['id']}}@else{{''}}@endif">

                    @include('layouts.admin.flash-message')

                                  @if(count($errors))
                                      <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <div style="padding: 10px;">
                                          <ul>
                                              @foreach($errors->all() as $error)
                                                  <li>{{$error}}</li>
                                              @endforeach
                                          </ul>
                                        </div>
                                      </div>
                                 @endif

                    @if($data['page'] == 'profile')


                            @if(isset($data['result']['Adm_Fullname']))
                                <?php $name = $data['result']['Adm_Fullname'];?>
                            @else
                                <?php $name = null;?>
                            @endif

                            @if(isset($data['result']['Adm_Username']))
                                <?php $username = $data['result']['Adm_Username'];?>
                            @else
                                <?php $username = null;?>
                            @endif

                            @if(isset($data['result']['Adm_Email']))
                                <?php $email = $data['result']['Adm_Email'];?>
                            @else
                                <?php $email = null;?>
                            @endif

                            @if(isset($data['result']['Adm_Tel']))
                                <?php $tel = $data['result']['Adm_Tel'];?>
                            @else
                                <?php $tel = null;?>
                            @endif


                        <div class="row">

                            <div class="col-lg-12">

                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="name" type="text" class="form-text" required value="{{$name}}">
                                    <label>Full Name</label>
                                </div>
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="username" type="text" class="form-text" required value="{{$username}}">
                                    <label>Username <span class="text-danger">*</span></label>
                                </div>
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="email" type="text" class="form-text" required value="{{$email}}">
                                    <label>Email <span class="text-danger">*</span></label>
                                </div>
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="tel" type="text" class="form-text" required value="{{$tel}}">
                                    <label>Tel. <span class="text-danger">*</span></label>
                                </div>

                            </div>

                            <div class="col-lg-12">

                                <button type="submit" class="btn btn-info btn-flat m-b-30 m-t-30">Save</button>
                                &nbsp;
                                <button type="reset" class="btn btn-danger btn-flat m-b-30 m-t-30">Reset</button>

                            </div>

                        </div>

                    @else

                        <div class="row">

                            <div class="col-lg-12">

                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="old_password" type="password" class="form-text" required >
                                    <label>Old Password <span class="text-danger">*</span></label>
                                </div>

                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="password" type="password" class="form-text" required >
                                    <label>New Password <span class="text-danger">*</span></label>
                                </div>
                                <div class="form-group form-animate-text" style="margin-top:40px !important;">
                                    <input name="password_confirmation" type="password" class="form-text" required >
                                    <label>Confirm Password <span class="text-danger">*</span></label>
                                </div>
                            </div>

                            <div class="col-lg-12">

                                <button type="submit" class="btn btn-info btn-flat m-b-30 m-t-30">Save</button>
                                &nbsp;
                                <button type="reset" class="btn btn-danger btn-flat m-b-30 m-t-30">Reset</button>

                            </div>

                        </div>

                    @endif

                    </form>

                </div>
</div>