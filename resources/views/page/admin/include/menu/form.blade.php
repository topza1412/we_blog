<div class="card-body">
                <div class="basic-elements">
                    <form action="{{url('admin/menu/'.$data['page'].'/action')}}" method="post" enctype="multipart/form-data">
                    @csrf

                    @include('layouts.admin.flash-message')

                                  @if(count($errors))
                                      <div class="alert alert-danger alert-block">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <div style="padding: 10px;">
                                          <ul>
                                              @foreach($errors->all() as $error)
                                                  <li>{{$error}}</li>
                                              @endforeach
                                          </ul>
                                        </div>
                                      </div>
                                 @endif

                    @if($data['page']=='about')
                    @foreach($data['detail'] as $value)
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <textarea name="detail" class="ckeditor form-control" required>{{$value->Abo_Detail}}</textarea>
                                </div>
                            
                            </div>

                            <div class="col-lg-12">

                                <button type="submit" class="btn btn-info btn-flat m-b-30 m-t-30">Save</button>
                                &nbsp;
                                <button type="reset" class="btn btn-danger btn-flat m-b-30 m-t-30">Reset</button>

                            </div>

                        </div>
                        @endforeach
                        @endif

                         @if($data['page']=='contact')
                        @foreach($data['detail'] as $value)
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                     <textarea name="detail" class="ckeditor form-control" required>{{$value->Con_Detail}}</textarea>
                                </div>
                            
                            </div>

                            <div class="col-lg-12">

                                <button type="submit" class="btn btn-info btn-flat m-b-30 m-t-30">Save</button>
                                &nbsp;
                                <button type="reset" class="btn btn-danger btn-flat m-b-30 m-t-30">Reset</button>

                            </div>

                        </div>
                        @endforeach
                        @endif


                    </form>

                    


                        

                </div>
            </div>