@extends('layouts.admin.template')

@section('detail') 

<!-- start: Content -->
            <div id="content">
               <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft">Menu</h3>
                        <p class="animated fadeInDown">
                          menu <span class="fa-angle-right fa"></span> <a href="{{url('admin/menu/'.$data['page'])}}">{{$data['page']}}</a>
                        </p>
                    </div>
                  </div>
              </div>
              <div class="col-md-12 top-20 padding-0">
                <div class="col-md-12">
                  <div class="panel">
                    <div class="panel-heading"><h3>Manage data</h3></div>
                    <div class="panel-body">

                        @include('page.admin.include.menu.form')

                  </div>
                </div>
              </div>  
              </div>
            </div>
<!-- end: content -->

@stop

