@extends('layouts.admin.template')

@section('detail') 

<div class="container-fluid mimin-wrapper">

<!-- start: content -->
<div id="content">
    <div class="col-md-12" style="padding:20px;">
        <div class="col-md-12 padding-0">
            <div class="col-md-12 padding-0">
                <div class="col-md-12">
                    <div class="panel box-v4">
                        <div class="panel-heading bg-white border-none">
                          <h4><span class="fa fa-area-chart"></span> Chart visit web</h4>
                        </div>
                            <div class="panel-body padding-0" style="padding: 10px;">
                              {!! $data['chart']['desktop']->html() !!}
                            </div>
                            <div class="panel-body padding-0" style="padding: 10px;">
                              {!! $data['chart']['mobile']->html() !!}
                            </div>
                            <div class="panel-body padding-0" style="padding: 10px;">
                              {!! $data['chart']['tablet']->html() !!}
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
  </div>
<!-- end: content -->

{!! Charts::scripts() !!}
{!! $data['chart']['desktop']->script() !!}
{!! $data['chart']['mobile']->script() !!}
{!! $data['chart']['tablet']->script() !!}

@stop

