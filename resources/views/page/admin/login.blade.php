<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="description" content="Miminium Admin Template v.1">
<meta name="author" content="Isna Nur Azis">
<meta name="keyword" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{{SettingWeb::SettingWeb()->Set_Title}}</title>

<!-- start: Css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/bootstrap.min.css')}}">

<!-- plugins -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/plugins/font-awesome.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/plugins/simple-line-icons.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/plugins/animate.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/plugins/icheck/skins/flat/aero.css')}}"/>
<link href="{{asset('assets/admin/css/style.css')}}" rel="stylesheet">

<link rel="shortcut icon" href="{{asset('upload/admin/shortcut_icon/icon.ico')}}">

</head>

    <body id="mimin" class="dashboard form-signin-wrapper">

      <div class="container">

          <form action="{{url('admin/login/auth')}}" class="form-signin" method="post">
          @csrf
          @include('layouts.admin.flash-message')

            @if(count($errors))
                <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <div style="padding: 10px;">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                  </div>
                </div>
           @endif
          <div class="panel periodic-login">
              <div class="panel-body text-center">
                  <img src="{{asset('upload/admin/logo_web/'.SettingWeb::SettingWeb()->Set_Logo)}}" width="120px">
                  <h2 class="atomic-symbol">{{SettingWeb::SettingWeb()->Set_Title}}</h2>
                  <i class="icons icon-arrow-down"></i>
                  <div class="form-group form-animate-text" style="margin-top:40px !important;">
                    <input type="text" name="username" class="form-text" required value="@if(isset($_COOKIE['remember_username'])){{$_COOKIE['remember_username']}}@endif">
                    <span class="bar"></span>
                    <label>Username</label>
                  </div>
                  <div class="form-group form-animate-text" style="margin-top:40px !important;">
                    <input type="password" name="password" class="form-text" required value="@if(isset($_COOKIE['remember_password'])){{$_COOKIE['remember_password']}}@endif">
                    <span class="bar"></span>
                    <label>Password</label>
                  </div>
                  <label class="pull-left">
                  <input type="checkbox" class="icheck pull-left" name="remember_login" @if(isset($_COOKIE['remember_login'])){{'checked'}}@endif> Remember me
                  </label>
                  <input type="submit" class="btn col-md-12" value="SignIn"/>
              </div>
                <div class="text-center" style="padding:5px;">
                    <a href="{{url('admin/forgotpassword')}}">Forgot Password </a>
                </div>
          </div>
        </form>

      </div>

      <!-- end: Content -->

</body>
</html>

<!-- js -->
<script src="{{asset('assets/admin/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/admin/js/jquery.ui.min.js')}}"></script>
<script src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/moment.min.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins/icheck.min.js')}}"></script>

<!-- custom -->
      <script src="{{asset('assets/admin/js/main.js"></script>
      <script type="text/javascript">
       $(document).ready(function(){
         $('input').iCheck({
          checkboxClass: 'icheckbox_flat-aero',
          radioClass: 'iradio_flat-aero'
        });
       });
     </script>
     <!-- end: Javascript -->
