@extends('layouts.member.template')

@section('detail') 
<!-- Start: Welcome Section -->
        <section class="welcome-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="welcome-wrap">
                            <div class="welcome-text">
                                <h2 class="section-title">Welcome to the Fameline libraria</h2>
                                <span class="underline left"></span>
                                <p class="lead">Website searching data for fameline company</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="facts-counter">
                            <ul>
                                <li class="bg-light-green">
                                    <div class="fact-item">
                                        <div class="fact-icon">
                                            <i class="ebook"></i>
                                        </div>
                                        <span>Core knowledge<strong class="fact-counter">{{$data['count_knowledge']['core']}}</strong></span>
                                    </div>
                                </li>
                                <li class="bg-green">
                                    <div class="fact-item">
                                        <div class="fact-icon">
                                            <i class="eaudio"></i>
                                        </div>
                                        <span>Advanced knowledge<strong class="fact-counter">{{$data['count_knowledge']['advance']}}</strong></span>
                                    </div>
                                </li>
                                <li class="bg-red">
                                    <div class="fact-item">
                                        <div class="fact-icon">
                                            <i class="magazine"></i>
                                        </div>
                                        <span>Innovation knowledge<strong class="fact-counter">{{$data['count_knowledge']['innovation']}}</strong></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="welcome-image"></div>
        </section>
        <!-- End: Welcome Section -->

<div id="content" class="site-content">
    <div id="primary" class="content-area">
    <main id="main" class="site-main">
    <div class="blog-main-list" style="background-color: #c3dd89; background-size:cover;">
    <div class="container">
    <div class="row">
    <div class="blog-page grid">
        <h2 align="center" style="color:#231f20;">Core Knowledge</h2>
        @if(count($data['result']['core'])>0)
            @foreach($data['result']['core'] as $value)
                <article>
                    <div class="grid-item blog-item">
                        <div class="post-thumbnail">
                            <div class="post-date-box">
                                <div class="post-date">
                                    <a class="date" href="#.">{{date("d",strtotime($value->created_at))}}</a>
                                </div>
                                <div class="post-date-month">
                                    <a class="month" href="#.">{{date("M",strtotime($value->created_at))}}</a>
                                </div>
                            </div>
                            <a href="{{url('knowledge/detail/'.$value->Kno_ID)}}"><img alt="blog" src="{{asset('upload/member/knowledge/thumbnail/core/'.$value->Kno_Thumbnail)}}" /></a>
                            <div class="post-share">
                                <a href="#."><i class="fa fa-comment"></i> {{$value->Kno_Comment}}</a>
                                <a href="#."><i class="fa fa-thumbs-o-up"></i> {{$value->Kno_Like}}</a>
                                <a href="#."><i class="fa fa-eye"></i> {{$value->Kno_View}}</a>
                            </div>
                        </div>
                        <div class="post-detail">
                            <header class="entry-header">
                                <h3 class="entry-title"><a href="{{url('knowledge/detail/'.$value->Kno_ID)}}">{{$value->Kno_Title}}</a></h3>
                                <div class="entry-meta">
                                    <span><i class="fa fa-user"></i> {{$value->Adm_Username}}</span>{{$value->Cat_Name}}
                                </div>
                            </header>
                            <div class="entry-content">
                                <p>{{$value->Kno_ShortContent}}</p>
                            </div>
                        </div>
                    </div>
                </article>
            @endforeach
        @else
            <div class="col-md-12 alert text-danger" align="center"><b>No data not found!</b><br></div>
        @endif
    </div>
    </div>
    </div>
    </div>
    </main>
    </div>
</div>


<div id="content" class="site-content">
    <div id="primary" class="content-area">
    <main id="main" class="site-main">
    <div class="blog-main-list" style="background-color: #7ac143; background-size:cover;">
    <div class="container">
    <div class="row">
    <div class="blog-page grid">
        <h2 align="center" style="color:#231f20;">Advance Knowledge</h2>
        @if(count($data['result']['advance'])>0)
            @foreach($data['result']['advance'] as $value)
                <article>
                    <div class="grid-item blog-item">
                        <div class="post-thumbnail">
                            <div class="post-date-box">
                                <div class="post-date">
                                    <a class="date" href="#.">{{date("d",strtotime($value->created_at))}}</a>
                                </div>
                                <div class="post-date-month">
                                    <a class="month" href="#.">{{date("M",strtotime($value->created_at))}}</a>
                                </div>
                            </div>
                            <a href="{{url('knowledge/detail/'.$value->Kno_ID)}}"><img alt="blog" src="{{asset('upload/member/knowledge/thumbnail/core/'.$value->Kno_Thumbnail)}}" /></a>
                            <div class="post-share">
                                <a href="#."><i class="fa fa-comment"></i> {{$value->Kno_Comment}}</a>
                                <a href="#."><i class="fa fa-thumbs-o-up"></i> {{$value->Kno_Like}}</a>
                                <a href="#."><i class="fa fa-eye"></i> {{$value->Kno_View}}</a>
                            </div>
                        </div>
                        <div class="post-detail">
                            <header class="entry-header">
                                <h3 class="entry-title"><a href="{{url('knowledge/detail/'.$value->Kno_ID)}}">{{$value->Kno_Title}}</a></h3>
                                <div class="entry-meta">
                                    <span><i class="fa fa-user"></i> {{$value->Adm_Username}}</span>{{$value->Cat_Name}}
                                </div>
                            </header>
                            <div class="entry-content">
                                <p>{{$value->Kno_ShortContent}}</p>
                            </div>
                        </div>
                    </div>
                </article>
            @endforeach
        @else
            <div class="col-md-12 alert text-danger" align="center"><b>No data not found!</b></div>
        @endif
    </div>
    </div>
    </div>
    </div>
    </main>
    </div>
</div>

@stop

