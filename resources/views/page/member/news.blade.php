@extends('layouts.member.template')

@section('detail') 
        <div id="content" class="site-content">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="blog-main-list">
                        <div class="container">
                            <br>
                            <h2 align="center" style="color:#231f20;">Knowledge</h2>
                            <p align="center"><b>Keyword search:</b> <span class="text-success">{{$data['keyword']}}</span></p>

                            <div class="row">

                                <div class="booksmedia-fullwidth">
                                <ul>
                                @if(count($data['result'])>0)
                                    @foreach($data['result'] as $value)
                                        @if($value->Gro_ID == 1)
                                            <?php $group = 'core';?>
                                        @elseif($value->Gro_ID == 2)
                                            <?php $group = 'advance';?>
                                        @elseif($value->Gro_ID == 3)
                                            <?php $group = 'innovation';?>
                                        @endif 
                                    <li>
                                        <figure>
                                            <a href="{{url('knowledge/detail/'.$value->Kno_ID)}}"><img src="{{asset('upload/member/knowledge/thumbnail/'.$group.'/'.$value->Kno_Thumbnail)}}" alt="Book"></a>
                                            <figcaption>
                                                <header>
                                                    <h4><a href="{{url('knowledge/detail/'.$value->Kno_ID)}}">{{$value->Kno_Title}}</a></h4>
                                                    <p><strong>Author:</strong>  {{$value->Adm_Username}}</p>
                                                    <p><strong>Date:</strong>  {{date("d-m-Y",strtotime($value->created_at))}}</p>
                                                </header>
                                                <p>{{$value->Kno_ShortContent}}</p>
                                            </figcaption>
                                        </figure>                                                
                                    </li>
                                    @endforeach
                                @else
                                    <div class="col-md-12 alert text-danger" align="center"><b>No data not found!</b><br></div>
                                @endif
                                    <div class="col-md-12 alert" align="center">{{$data['result']->links()}}<br></div>
                                </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
@stop

