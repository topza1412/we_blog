@extends('layouts.member.template')

@section('detail') 
<!-- Start: Knowledge Section -->
        <div id="content" class="site-content">
            <br>
            <br>
            <div id="primary" class="content-area">
                @if($data['result'] != null)
                    @if($data['result']->Gro_ID == 1)
                        <?php $group = 'core';?>
                    @elseif($data['result']->Gro_ID == 2)
                        <?php $group = 'advance';?>
                    @elseif($data['result']->Gro_ID == 3)
                        <?php $group = 'innovation';?>
                    @endif 
                        <main id="main" class="site-main">
                            <div class="booksmedia-detail-main">
                                <div class="container">

                                    <h1>{{$data['result']->Kno_Title}}</h1>
                                    <br>
                                    <div class="booksmedia-detail-box">
                                        <div class="detailed-box">
                                            <div class="col-xs-12 col-sm-5 col-md-3">
                                                <div class="post-thumbnail">
                                                    <img src="{{asset('upload/member/knowledge/thumbnail/'.$group.'/'.$data['result']->Kno_Thumbnail)}}" alt="Book Image">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-7 col-md-6">
                                                <div class="post-center-content">
                                                    <p><strong>Author:</strong> {{$data['result']->Adm_Username}}</p>
                                                    <p><strong>Group:</strong> {{$group}}</p>
                                                    <p><strong>Category :</strong> {{$data['result']->Cat_Name}}</p>
                                                    <p><strong>Date:</strong> {{date("d-m-Y",strtotime($data['result']->created_at))}}</p>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-3 ">
                                                <div class="post-right-content">
                                                    <p><i class="fa fa-eye"></i> {{$data['result']->Kno_View}} Viewed</p>
                                                    <p><i class="fa fa-thumbs-o-up"></i> {{$data['result']->Kno_Like}} Likes</p>
                                                    <p><i class="fa fa-comment"></i> {{$data['comment']['count']}} Comments</a></p>
                                                    <br>
                                                    <p><a href="{{url('knowledge/like/'.$data['result']->Kno_ID)}}" class="btn btn-warning">Like</a></p>
                                                    @if(session('session_id'))
                                                        <p><a href="{{url('knowledge/favorite/'.$data['result']->Kno_ID)}}" class="btn btn-primary">Favorite</a></p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>

                                        <p>{{$data['result']->Kno_ShortContent}}</p>

                                        <b>Tags:</b>
                                        @foreach(json_decode($data['result']->Kno_Tags) as $value)
                                            {{$value}}
                                        @endforeach

                                        <hr>

                                        <div class="table-tabs" id="responsiveTabs">
                                            <ul class="nav nav-tabs">
                                                <li class="active"><b class="arrow-up"></b><a data-toggle="tab" href="#sectionA">Detail</a></li>
                                                <li><b class="arrow-up"></b><a data-toggle="tab" href="#sectionB">Gallery</a></li>
                                                <li><b class="arrow-up"></b><a data-toggle="tab" href="#sectionC">Video</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                <div id="sectionA" class="tab-pane fade in active">
                                                    <p align="center">
                                                        {!! $data['result']->Kno_FullContent !!}
                                                    </p>
                                                </div>
                                                <div id="sectionB" class="tab-pane fade in">
                                                    @if($data['result']->Kno_Galleries != null)
                                                        @foreach(json_decode($data['result']->Kno_Galleries) as $value)
                                                    <p align="center">
                                                        <img src="{{asset('upload/member/knowledge/galleries/'.$group.'/'.$value)}}" alt="{{$value}}">
                                                    </p>
                                                        @endforeach
                                                    @else
                                                        <div class="col-md-12 alert text-danger" align="center"><b>No data not found!</b><br></div>
                                                    @endif
                                                </div>
                                                <div id="sectionC" class="tab-pane fade in">
                                                @if($data['result']->Kno_VideoFile != null)
                                                    <p align="center">
                                                        <video poster="{{asset('upload/member/knowledge/thumbnail/'.$group.'/'.$data['result']->Kno_Thumbnail)}}" id="player" playsinline controls width="700">
                                                         <source src="{{asset('upload/member/knowledge/video/'.$group.'/'.$data['result']->Kno_VideoFile)}}" type="video/mp4">
                                                         <source src="{{asset('upload/member/knowledge/video/'.$group.'/'.$data['result']->Kno_VideoFile)}}" type="video/webm">
                                                         <track kind="captions" label="Video preview" src="{{asset('upload/member/knowledge/video/'.$group.'/'.$data['result']->Kno_VideoFile)}}" srclang="en" default>
                                                        </video>
                                                    </p>
                                                @else
                                                    <div class="col-md-12 alert text-danger" align="center"><b>No data not found!</b><br></div>
                                                @endif
                                                </div>                                                   
                                            </div>
                                        </div>
                                    </div>
                                    <div class="comments-area" id="comments">
                                            <div class="comment-bg">
                                                <h4 class="comments-title">Comments</h4>
                                                <span class="underline left"></span>
                                                <ol class="comment-list">
                                                    @if(count($data['comment']['data'])>0)
                                                        @foreach($data['comment']['data'] as $value)
                                                            <li class="comment even thread-even depth-1 parent">
                                                                <div class="comment-body">
                                                                    <div class="comment-author vcard">
                                                                        <b class="fn">
                                                                            {{$value->Use_Username}}
                                                                        </b>                    
                                                                    </div>
                                                                    <footer class="comment-meta">
                                                                        <div class="left-arrow"></div>
                                                                        <div class="comment-metadata">
                                                                            <a href="#">
                                                                                <time datetime="2016-01-17">
                                                                                    <b>{{date("d-m-Y",strtotime($value->created_at))}}</b>
                                                                                </time>
                                                                            </a>
                                                                        </div>
                                                                        <div class="comment-content">
                                                                            <p>{{$value->Com_Detail}}</p>
                                                                        </div>
                                                                    </footer>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    @else
                                                        <div class="col-md-12 alert text-danger" align="center"><b>No data not found!</b><br></div>
                                                    @endif
                                                </ol>
                                            </div>
                                            <div class="comment-respond" id="respond">
                                                @include('layouts.member.flash-message')
                                                @if(count($errors))
                                                    <div class="alert alert-danger alert-block">
                                                      <button type="button" class="close" data-dismiss="alert">×</button>
                                                      <div style="padding: 10px;">
                                                        <ul>
                                                            @foreach($errors->all() as $error)
                                                                <li>{{$error}}</li>
                                                            @endforeach
                                                        </ul>
                                                      </div>
                                                    </div>
                                               @endif
                                                <h4 class="comment-reply-title" id="reply-title">New comment:</h4>
                                                <span class="underline left"></span>
                                                @if(session('session_id'))
                                                    <form class="comment-form" id="commentform" method="post" action="{{url('knowledge/comment')}}">
                                                        @csrf
                                                        <input type="hidden" name="knowledge_id" value="{{$data['result']->Kno_ID}}">
                                                        <div class="row">
                                                            <p class="comment-form-comment">
                                                                <textarea name="comment" id="comment" required placeholder="Commemt detail"></textarea>
                                                            </p>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <p class="form-submit">
                                                            <input value="Submit" class="submit" id="submit" name="submit" type="submit"> 
                                                        </p>
                                                    </form>
                                                @else
                                                    <div class="col-md-12 alert text-danger" align="center">Please login for comment.</div>
                                                @endif
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </main>
                @else
                    <div class="col-md-12 alert text-danger" align="center"><b>No data not found!</b><br></div>
                @endif
            </div>
        </div>
        <!-- End: Knowledge Section -->
@stop

