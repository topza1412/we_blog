@extends('layouts.member.template')

@section('detail')

@if($data['result'][0]->Gro_ID == 1)
    <?php $group = 'core';?>
@elseif($data['result'][0]->Gro_ID == 2)
    <?php $group = 'advance';?>
@elseif($data['result'][0]->Gro_ID == 3)
    <?php $group = 'innovation';?>
@endif 

<div id="content" class="site-content">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="books-media-gird">
                        <div class="container">
                            <br>
                            <div class="row">
                                <div class="col-md-9 col-md-push-3">
                                    <h3>{{$data['result'][0]->Cat_Name}}</h3>
                                    <hr>
                                    <div class="books-gird">
                                        <ul>
                                        @if(count($data['result'])>0)
                                            @foreach($data['result'] as $value)
                                            <li>
                                                <figure>
                                                    <img src="{{asset('upload/member/knowledge/thumbnail/'.$group.'/'.$value->Kno_Thumbnail)}}" alt="Book">
                                                    <figcaption>
                                                        <p><strong>{{$value->Kno_Title}}</strong></p>
                                                        <p><strong>Date:</strong> {{date("d-m-Y",strtotime($value->created_at))}}</p>
                                                        <p><strong>Author:</strong>  {{$value->Adm_Username}}</p>
                                                    </figcaption>
                                                </figure> 
                                                <div class="single-book-box">
                                                    <div class="post-detail">
                                                        <header class="entry-header">
                                                            <h3 class="entry-title"><a href="{{url('knowledge/detail/'.$value->Kno_ID)}}">{{$value->Kno_Title}}</a></h3>
                                                            <ul>
                                                                <li><strong>Author:</strong> {{$value->Adm_Username}}</li>
                                                            </ul>
                                                        </header>
                                                        <div class="entry-content">
                                                            <p>{{$value->Kno_ShortContent}}</p>
                                                        </div>
                                                        <footer class="entry-footer">
                                                            <a class="btn btn-primary" href="{{url('knowledge/detail/'.$value->Kno_ID)}}">Read More</a>
                                                        </footer>
                                                    </div>
                                                </div>                                       
                                            </li>
                                            @endforeach
                                        @else
                                            <div class="col-md-12 alert text-danger" align="center"><b>No data not found!</b><br></div>
                                        @endif
                                        </ul>
                                    </div>
                                    <nav class="navigation pagination text-center">
                                        <div class="nav-links">
                                            {{$data['result']->links()}}
                                            <br>
                                        </div>
                                    </nav>
                                </div>
                                <div class="col-md-3 col-md-pull-9">
                                    <aside id="secondary" class="sidebar widget-area" data-accordion-group>
                                        <div class="widget widget_recent_releases">
                                            <h4 class="widget-title">Categories</h4>
                                            <ul>
                                                @if(count($data['categories'])>0)
                                                    @foreach($data['categories'] as $value)
                                                        <li><a href="{{$value->Cat_ID}}">{{$value->Cat_Name}}</a></li>
                                                    @endforeach
                                                @endif

                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="widget widget_recent_entries">
                                            <h4 class="widget-title">Knowledge Releases</h4>
                                            <ul>
                                                @if(count($data['releases'])>0)
                                                    @foreach($data['releases'] as $value)
                                                <li>
                                                    <figure>
                                                        <img src="{{asset('upload/member/knowledge/thumbnail/'.$group.'/'.$value->Kno_Thumbnail)}}" alt="product" />
                                                    </figure>
                                                    <a href="{{url('knowledge/detail/'.$value->Kno_ID)}}">{{$value->Kno_Title}}</a>
                                                    <span class="price"><strong>Date:</strong> {{date("d-m-Y",strtotime($value->created_at))}}</span>
                                                    <div class="clearfix"></div>
                                                </li>
                                                    @endforeach
                                                @else
                                                    <div class="col-md-12 alert text-danger" align="center"><b>No data not found!</b><br></div>
                                                @endif
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                    </aside>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
@stop

