<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Admin;

class AdminController extends Controller
{ 
    public function __construct()
    {

    }

    public function page($page,Request $request){

      $detail = Admin::where('Adm_Status',0)->orderby('Adm_ID','desc')->get();

      $data = ['page' => $page,'detail' => $detail,'type' => 'view'];

      return view('page.admin.member',['data' => $data]);
    }


    public function form_add ($page) {

      $data = ['page' => $page,'detail' => null,'type' => 'form','action' => 'insert'];

      return view('page.admin.member',['data' => $data]);

    }



    public function form_edit ($page,$id) {

      $detail = Admin::where('Adm_ID',$id)->first();
      $data = ['page' => $page,'detail' => $detail,'type' => 'form','action' => 'update','id' => $id];

      return view('page.admin.member',['data' => $data]);

    }


    public function action ($page,Request $request) {

    if($request->post()){

              $this->validate(request(), [
              'name' => 'required|string|max:255',
              'adm_username' => 'required|string|unique:admin|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
              'password' => 'required|string|min:6',
              'adm_email' => 'required|string|unique:admin|email|max:255',
              'tel' => 'required|digits:10|numeric',
              ]);

          if($request->type_action=='insert'){

            $result = new Admin;
            $result->Adm_Fullname = $request->name;
            $result->Adm_Username = $request->username;
            $result->Adm_Password = md5($request->password);
            $result->Adm_Email = $request->email;
            $result->Adm_Tel = $request->tel;
            $result->Adm_Department = $request->department;
            $result->save();

            $result = new RoleAdmin;
            $result->Adm_ID = Admin::max('Adm_ID');
            $result->save();

            $this->LogsSystem('admin', 'insert', $request->username);

            $alert_success = 'add_success';
            $alert_not_success = 'add_not_success';

          } else if($request->type_action=='update'){

            	$data = [
            	'Adm_Fullname' => $request->name,
            	'Adm_Username' => $request->username,
            	'Adm_Email' => $request->email,
            	'Adm_Tel' => $request->tel,
              'Adm_Department' => $request->department,
            	'Adm_Permission' => $request->permission,
              'updated_at' => now(),
            	];
            	$result = Admin::where('Adm_ID',$request->id)->update($data);

              $alert_success = 'edit_success';
              $alert_not_success = 'edit_not_success';
          }

      	if($result){
      	   return back()->with('success',trans('other.'.$alert_success));
      	}
      	else{
      	   return back()->with('error',trans('other.'.$alert_not_success));
      	}

  }

}

    public function delete ($page,$id) {

  	   $result = Admin::where('Adm_ID',$id)->delete();

    	if($result){
    	   return back()->with('success',trans('other.delete_success'));
    	}
    	else{
    	   return back()->with('error',trans('other.delete_not_success'));
    	}

    }



}
