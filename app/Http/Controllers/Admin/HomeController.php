<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Charts;
use App\User;
use App\News;

class HomeController extends Controller
{

    public function __construct()
    {

    }

    public function index()
    {   
            $visit_web['desktop'] = DB::table('visit_web')
            ->select('*')
            ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"), "=", date('Y'))
            ->where('Viw_Device','Desktop')
            ->get();

            $visit_web['mobile'] = DB::table('visit_web')
            ->select('*')
            ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"), "=", date('Y'))
            ->whereIn('Viw_Device',['iPhone','Android'])
            ->get();

            $visit_web['tablet'] = DB::table('visit_web')
            ->select('*')
            ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"), "=", date('Y'))
            ->whereIn('Viw_Device',['iPad','tablet'])
            ->get();

            $chart['desktop'] = Charts::database($visit_web['desktop'], 'bar', 'highcharts')
                  ->title("Monthly visitor desktop")
                  ->elementLabel("Total visitor desktop")
                  ->dimensions(1100, 500)
                  ->responsive(true)
                  ->groupByMonth(date('Y'), true);

            $chart['mobile'] = Charts::database($visit_web['mobile'], 'bar', 'highcharts')
                  ->title("Monthly visitor mobile")
                  ->elementLabel("Total visitor mobile")
                  ->dimensions(1100, 500)
                  ->responsive(true)
                  ->groupByMonth(date('Y'), true);

            $chart['tablet'] = Charts::database($visit_web['tablet'], 'bar', 'highcharts')
                  ->title("Monthly visitor tablet")
                  ->elementLabel("Total visitor tablet")
                  ->dimensions(1100, 500)
                  ->responsive(true)
                  ->groupByMonth(date('Y'), true);

        $data = ['chart' => $chart];

        return view('page.admin.index',['data' => $data]);
    }
}
