<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Setting;
use App\SlideBanner;

class SettingController extends Controller
{

  public function __construct()
    {

    }
    
    public function page($page){

        if($page=='website' || $page=='email' || $page=='seo'){
          $result = Setting::all();
    	  } else if($page=='slider'){
      	  $result = SlideBanner::all();
    	  }


      $data =  ['page' => $page,'result' => $result,'type' => 'view'];

      return view('page.admin.setting',['data' => $data]);

    }


    public function form_add ($page) {

      if($page=='slider'){
    	  $slider = null;
    	  $data = ['page' => 'slider','detail' => $slider,'type' => 'form','action' => 'insert'];
  	  }

      return view('page.admin.setting',['data' => $data]);

    }


    public function form_edit ($page,$id) {

  	  if($page=='slider'){
      	  $slider = SlideBanner::where('Sli_ID',$id)->first();
      	  $data = ['page' => 'slider','detail' => $slider,'type' => 'form','action' => 'update'];
  	  }

      return view('page.admin.setting',['data' => $data]);

    }

    public function action ($page,Request $request) {

    	if($page=='website'){

        $this->validate(request(), [
          'email_admin' => 'required|string|email|max:255',
          'logo_web' => 'mimes:jpeg,jpg,png|max:2048']);

          	if($request->logo_web==null){
          	    $file_upload = $request->logo_web_hidden;
          	} else{
                $file_upload = $this->Upload_File($request->file('logo_web'), 'upload/admin/logo_web/', 200, 220);
            }

              	$data = [
              	'Set_Title' => $request->title_web,
              	'Set_Logo' => $file_upload,
              	'Set_EmailAdmin' => $request->email_admin,
              	'Set_StatusWeb' => $request->status_web,
              	];
              	$result = Setting::where('Set_ID','1')->update($data);
                
                $alert_success = 'edit_success';
                $alert_not_success = 'edit_not_success';

      } else if($page=='slider'){
          $this->validate(request(), ['slide_img' => 'mimes:jpeg,jpg,png|max:2048']);
        	if($request->slide_img==null){
              $slide_img = $request->slide_img_hidden;
          } else{
              $slide_img = $this->Upload_File($request->file('slide_img'), 'upload/member/slide/', 1280, 650);
          } if($request->type_action=='insert'){
              	$result = new SlideBanner;
              	$result->Sli_Name = $request->name;
              	$result->Sli_Img = $slide_img;
                $result->Sli_Status = 1;
              	$result->created_at = now();
              	$result->created_at = now();
              	$result->save();
                $this->LogsSystem('slider', 'insert', $request->name);
                $alert_success = 'add_success';
                $alert_not_success = 'add_not_success';
        	} else{
              	$data = [
              	'Sli_Name' => $request->name,
              	'Sli_Img' => $slide_img,
                'Sli_Status' => $request->status,
              	'updated_at' => now(),
              	];
              	$result = SlideBanner::where('Sli_ID',$request->id)->update($data);

                $alert_success = 'edit_success';
                $alert_not_success = 'edit_not_success';
          }
      }

      	if($result){
      	   return back()->with('success',trans('other.'.$alert_success));
      	} else{
      	   return back()->with('error',trans('other.'.$alert_not_success));
      	}

}


    public function delete ($page,$id) {

    	if($page=='slider'){
      	 $result = SlideBanner::where('Sli_ID',$id)->delete();
    	}

    	if($result){
    	   return back()->with('success',trans('other.delete_success'));
    	} else{
    	   return back()->with('error',trans('other.delete_not_success'));
    	}

    }

}
