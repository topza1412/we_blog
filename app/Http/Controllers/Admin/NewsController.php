<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Charts;
use App\Categories;
use App\Knowledge;
use App\CommentKnowledge;
use App\Admin;
use App\LogsKnowledge;
use App\LogsSystem;
use App\Section;
use App\SectionDetail;

class KnowledgeController extends Controller
{ 
    public function __construct()
    {
        
    }

    public function page($page,Request $request){

      if($page=='core'){
          $result = Knowledge::join('admin', 'admin.Adm_ID', '=', 'knowledge.Adm_ID')
          ->join('categories', 'categories.Cat_ID', '=', 'knowledge.Cat_ID')
          ->where('knowledge.Gro_ID',1);

          if(session('session_admin_status') != 1){
            $result = $result->where('knowledge.Adm_ID', session('session_admin_id'));
          } 

          $result = $result->orderby('knowledge.Kno_ID','desc')->get();

  	  } else if($page=='advance'){
      	  $result = Knowledge::join('admin', 'admin.Adm_ID', '=', 'knowledge.Adm_ID')
          ->join('categories', 'categories.Cat_ID', '=', 'knowledge.Cat_ID')
          ->where('knowledge.Gro_ID',2);

          if(session('session_admin_status') != 1){
            $result = $result->where('knowledge.Adm_ID', session('session_admin_id'));
          } 

          $result = $result->orderby('knowledge.Kno_ID','desc')->get();

  	  } else if($page=='innovation'){
          $result = Knowledge::join('admin', 'admin.Adm_ID', '=', 'knowledge.Adm_ID')
          ->join('categories', 'categories.Cat_ID', '=', 'knowledge.Cat_ID')
          ->where('knowledge.Gro_ID',3);

          if(session('session_admin_status') != 1){
            $result = $result->where('knowledge.Adm_ID', session('session_admin_id'));
          } 

          $result = $result->orderby('knowledge.Kno_ID','desc')->get();
      }

      $data = ['page' => $page,'result' => $result,'type' => 'view'];

      return view('page.admin.knowledge',['data' => $data]);
    }


    public function form_add ($page) {

      $categories = Categories::where('Cat_Status',1)->get();

      $section = Section::where('Sec_Status',1)->get();
      
      $data = ['page' => $page,'result' => null, 'categories' => $categories, 'section' => $section, 'type' => 'form','action' => 'insert'];

      return view('page.admin.knowledge',['data' => $data]);

    }



    public function form_edit ($page,$id) {

      $result = Knowledge::join('admin', 'admin.Adm_ID', '=', 'knowledge.Adm_ID')
                ->join('categories', 'categories.Cat_ID', '=', 'knowledge.Cat_ID')
                ->where('knowledge.Kno_ID',$id)
                ->first();

      $categories = Categories::where('Cat_Status',1)->get();

      $section = Section::where('Sec_Status',1)->get();

      $data = ['page' => $page,'result' => $result, 'categories' => $categories, 'section' => $section, 'type' => 'form','action' => 'update','id' => $id];

      return view('page.admin.knowledge',['data' => $data]);

    }


    public function action ($page,Request $request) {

    if($request->post()){

            $this->validate(request(), [
            'full_content' => 'required|string',
            'thumbnail' => 'mimes:jpeg,jpg,png|max:2048',
            'img_gallery1' => 'mimes:jpeg,jpg,png|max:2048',
            'img_gallery2' => 'mimes:jpeg,jpg,png|max:2048',
            'img_gallery3' => 'mimes:jpeg,jpg,png|max:2048',
            'img_gallery4' => 'mimes:jpeg,jpg,png|max:2048',
            'img_gallery5' => 'mimes:jpeg,jpg,png|max:2048',
            'img_gallery6' => 'mimes:jpeg,jpg,png|max:2048',
            'img_gallery7' => 'mimes:jpeg,jpg,png|max:2048',
            'img_gallery8' => 'mimes:jpeg,jpg,png|max:2048',
            'img_gallery9' => 'mimes:jpeg,jpg,png|max:2048',
            'img_gallery10' => 'mimes:jpeg,jpg,png|max:2048',
            'video_file' => 'mimes:mp4,mpeg|max:51200',
            ]);

            if($request->group == 1){
              $subfolder = 'core';
            } else if($request->group == 2){
              $subfolder = 'advancce';
            } else if($request->group == 3){
              $subfolder = 'innovation';
            } 

            if($request->thumbnail == null){
              $thumbnail = $request->thumbnail_hidden;
            } else{
              $thumbnail = $this->Upload_File($request->file('thumbnail'), 'upload/member/knowledge/thumbnail/'.$subfolder.'/', 960, 650);
            }

            if($request->img_gallery1 == null){
              $img_gallery1 = $request->img_gallery1_hidden;
            } else{
              $img_gallery1 = $this->Upload_File($request->file('img_gallery1'), 'upload/member/knowledge/galleries/'.$subfolder.'/', 960, 650);
            }

            if($request->img_gallery2 == null){
              $img_gallery2 = $request->img_gallery2_hidden;
            } else{
              $img_gallery2 = $this->Upload_File($request->file('img_gallery2'), 'upload/member/knowledge/galleries/'.$subfolder.'/', 960, 650);
            }

            if($request->img_gallery3 == null){
              $img_gallery3 = $request->img_gallery3_hidden;
            } else{
              $img_gallery3 = $this->Upload_File($request->file('img_gallery3'), 'upload/member/knowledge/galleries/'.$subfolder.'/', 960, 650);
            }

            if($request->img_gallery4 == null){
              $img_gallery4 = $request->img_gallery4_hidden;
            } else{
              $img_gallery4 = $this->Upload_File($request->file('img_gallery4'), 'upload/member/knowledge/galleries/'.$subfolder.'/', 960, 650);
            }

            if($request->img_gallery5 == null){
              $img_gallery5 = $request->img_gallery5_hidden;
            } else{
              $img_gallery5 = $this->Upload_File($request->file('img_gallery5'), 'upload/member/knowledge/galleries/'.$subfolder.'/', 960, 650);
            }

            if($request->img_gallery6 == null){
              $img_gallery6 = $request->img_gallery6_hidden;
            } else{
              $img_gallery6 = $this->Upload_File($request->file('img_gallery6'), 'upload/member/knowledge/galleries/'.$subfolder.'/', 960, 650);
            }

            if($request->img_gallery7 == null){
              $img_gallery7 = $request->img_gallery6_hidden;
            } else{
              $img_gallery7 = $this->Upload_File($request->file('img_gallery7'), 'upload/member/knowledge/galleries/'.$subfolder.'/', 960, 650);
            }

            if($request->img_gallery8 == null){
              $img_gallery8 = $request->img_gallery8_hidden;
            } else{
              $img_gallery8 = $this->Upload_File($request->file('img_gallery8'), 'upload/member/knowledge/galleries/'.$subfolder.'/', 960, 650);
            }

            if($request->img_gallery9 == null){
              $img_gallery9 = $request->img_gallery9_hidden;
            } else{
              $img_gallery9 = $this->Upload_File($request->file('img_gallery9'), 'upload/member/knowledge/galleries/'.$subfolder.'/', 960, 650);
            }

            if($request->img_gallery10 == null){
              $img_gallery10 = $request->img_gallery10_hidden;
            } else{
              $img_gallery10 = $this->Upload_File($request->file('img_gallery10'), 'upload/member/knowledge/galleries/'.$subfolder.'/', 960, 650);
            }

            if($request->video_file == null){
              $video_file_name = $request->video_file_hidden;
            } else{
              $video_file = $request->file('video_file');
              $video_file_name = Date("dmy_His").'.'.$video_file->getClientOriginalExtension();
              $video_file->move('upload/member/knowledge/video/'.$subfolder.'/',$video_file_name);
            }

            $galleries = ['img_gallery1' => $img_gallery1, 'img_gallery2' => $img_gallery2, 'img_gallery3' => $img_gallery3, 'img_gallery4' => $img_gallery4, 'img_gallery5' => $img_gallery5, 'img_gallery6' => $img_gallery6, 'img_gallery7' => $img_gallery7, 'img_gallery8' => $img_gallery8, 'img_gallery9' => $img_gallery9, 'img_gallery10' => $img_gallery10];


            if($request->type_action=='insert'){

                $result = new Knowledge;
                $result->Adm_ID = session('session_admin_id');
                $result->Kno_thumbnail = $thumbnail;
                $result->Kno_Title = $request->title;
                $result->Gro_ID = $request->group;
                $result->Cat_ID = $request->categories;
                $result->Sec_ID = (isset($request->section)) ? $request->section : false;
                $result->Kno_ShortContent = $request->short_content;
                $result->Kno_FullContent = $request->full_content;
                $result->Kno_Tags = json_encode($request->tags);
                $result->Kno_Galleries = json_encode($galleries);
                $result->Kno_VideoFile = $video_file_name;
                $result->Kno_Status = 1;
                $result->save();

                $this->LogsKnowledge(Knowledge::max('Kno_ID'));
                $this->LogsSystem('knowledge', 'insert', $request->title);

                $alert_success = 'add_success';
                $alert_not_success = 'add_not_success';

            } else if($request->type_action=='update'){

              	$data = [
                'Adm_ID' => session('session_admin_id'),
              	'Kno_Thumbnail' => $thumbnail,
              	'Kno_Title' => $request->title,
                'Gro_ID' => $request->group,
                'Cat_ID' => $request->categories,
                'Sec_ID' => (isset($request->section)) ? $request->section : false,
                'Kno_ShortContent' => $request->short_content,
                'Kno_FullContent' => $request->full_content,
                'Kno_Tags' => json_encode($request->tags),
                'Kno_Galleries' => json_encode($galleries),
                'Kno_VideoFile' => $video_file_name,
                'Kno_Status' => $request->status,
                'updated_at' => now(),
              	];
              	$result = Knowledge::where('Kno_ID',$request->id)->update($data);

                $this->LogsKnowledge($request->id);
                $this->LogsSystem('knowledge', 'update', $request->title);

                $alert_success = 'edit_success';
                $alert_not_success = 'edit_not_success';
            }

          	if($result){
          	   return back()->with('success',trans('other.'.$alert_success));
          	} else{
          	   return back()->with('error',trans('other.'.$alert_not_success));
          	}
    }

  }

    public function delete ($page,$id) {

    	$result = Knowledge::where('Kno_ID',$id)->delete();

    	if($result){
    	   return back()->with('success',trans('other.delete_success'));
         $this->LogsKnowledge($id);
         $this->LogsSystem('knowledge', 'delete', 'knowledge id '.$id);

    	}
    	else{
    	   return back()->with('error',trans('other.delete_not_success'));
    	}

    }

    public function report ($id) {

      $title = Knowledge::find($id);

      $result['view'] = Knowledge::select('Kno_View')->where('Kno_ID',$id)->first();

      $result['like'] = Knowledge::select('Kno_Like')->where('Kno_ID',$id)->first();

      $result['comment'] = CommentKnowledge::where('Kno_ID',$id)->count();

      $data = ['page' => 'report','title' => $title, 'result' => $result, 'type' => 'report'];

      return view('page.admin.knowledge',['data' => $data]);

    }

}
