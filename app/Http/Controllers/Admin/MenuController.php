<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Home;
use App\About;
use App\Contact;

class MenuController extends Controller
{

    public function __construct()
    {
        $this->middleware('session_login_admin');
    }
    
    public function page($page){

      if($page=='about'){
        $detail = About::all();
      } else if($page=='contact'){
    	  $detail = Contact::all();
  	  }

      $data = ['page' => $page,'detail' => $detail];

      return view('page.admin.menu',['data' => $data]);

}



    public function action ($page,Request $request) {

      if($request->post()){

        if($page=='about'){
        	$data = [
        	'Abo_Detail' => ($request->detail != null) ? $request->detail : null
        	];
        	$result = DB::table('about')->update($data);

          $alert_success = 'edit_success';
          $alert_not_success = 'edit_not_success';
      	} else if($page=='contact'){
          $data = [
          'Con_Detail' => ($request->detail != null) ? $request->detail : null
          ];
          
          $result = DB::table('contact')->update($data);

          $alert_success = 'edit_success';
          $alert_not_success = 'edit_not_success';
        }


      	if($result){
      	   return back()->with('success',trans('other.'.$alert_success));
      	} else{
      	   return back()->with('error',trans('other.'.$alert_not_success));
      	}

       }


}


}
