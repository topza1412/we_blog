<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Knowledge;
use App\FavoriteKnowledge;
use App\CommentKnowledge;

class KnowledgeController extends Controller
{
    public function search(Request $request) {

        $result = Knowledge::join('admin', 'admin.Adm_ID', '=', 'knowledge.Adm_ID')
        ->join('categories', 'categories.Cat_ID', '=', 'knowledge.Cat_ID');

        if($request->txt_search && $request->group){
          $result = $result->where('knowledge.Kno_Title', 'like', '%' .$request->txt_search. '%')
          ->where('knowledge.Gro_ID', 'like', '%' .$request->group. '%');
        }

        $result = $result->where('knowledge.Kno_Title', 'like', '%' .$request->txt_search. '%')
        ->orWhere('knowledge.Kno_ShortContent', 'like', '%' .$request->txt_search. '%')
        ->orWhere('knowledge.Kno_FullContent', 'like', '%' .$request->txt_search. '%')
        ->orWhere('knowledge.Kno_Tags', 'like', '%' .$request->txt_search. '%')
        ->orderby('knowledge.created_at','desc')
        ->paginate(12);

        if($request->group != null){
          if($request->group == 1){
            $group = 'Core knowledge';
          } else if($request->group == 2){
            $group = 'Advance knowledge';
          } else if($request->group == 3){
            $group = 'Innovation knowledge';
          } 
        } else {
          $group = null;
        }

        if($request->txt_search && $request->group){
          $keyword = $request->txt_search.' ('.$group.')';
        } else {
          $keyword = $request->txt_search;
        }

        //seo
        $seo = $this->Seo(null,'Knowledge','Knowledge','Knowledge','Knowledge',url('knowledge'));

        $data = array('result' => $result,'keyword' => $keyword, 'seo' => $seo);

        return view('page.member.knowledge',['data' => $data]);

    }


    public function detail($id = null) {

        $result = Knowledge::join('admin', 'admin.Adm_ID', '=', 'knowledge.Adm_ID')
        ->join('categories', 'categories.Cat_ID', '=', 'knowledge.Cat_ID')
        ->where('knowledge.Kno_ID', $id)
        ->first();

        $comment['data'] = Knowledge::join('comment_knowledge', 'comment_knowledge.Kno_ID', '=', 'knowledge.Kno_ID')
        ->join('user', 'user.Use_ID', '=', 'comment_knowledge.Use_ID')
        ->where('comment_knowledge.Kno_ID', $id)
        ->orderby('comment_knowledge.Com_ID', 'desc')
        ->paginate(10);

        $comment['count'] = Knowledge::join('comment_knowledge', 'comment_knowledge.Kno_ID', '=', 'knowledge.Kno_ID')
        ->join('user', 'user.Use_ID', '=', 'comment_knowledge.Use_ID')
        ->where('comment_knowledge.Kno_ID', $id)
        ->count();

        if($result->Kno_Title != null){

          if($result->Gro_ID == 1){
            $group = 'core';
          } else if($result->Gro_ID == 2){
            $group = 'advance';
          } else if($result->Gro_ID == 3) {
            $group = 'innovation';
          }
          
          //seo detail
          $logo = asset('upload/member/knowledge/thumbnail/'.$group.'/'.$result->Kno_Thumbnail);
          $title = $result->Kno_Title;
          $description = $result->Kno_ShortContent;

        } else {

          $title = 'Knowledge';

        }

        $addView = Knowledge::find($id);
        $addView->Kno_View = ($addView->Kno_View)+1;
        $addView->save();

        //seo
        $seo = $this->Seo($logo,$title,$description,$title,$title,url('knowledge/detail/'.$result->Kno_ID));

        $data = array('result' => $result, 'comment' => $comment, 'seo' => $seo);

        return view('page.member.knowledge_detail',['data' => $data]);

    }

    public function like($id) {

        $result = Knowledge::find($id);
        $result->Kno_Like = ($result->Kno_Like)+1;
        $result->save();

        return back()->with('success','Like success');

    }

    public function favorite($id) {

        if(FavoriteKnowledge::where(['Kno_ID' => $id, 'Use_ID' => session('session_id')])->first() != null){

          $result = ['result' => false,'message' => 'You have favorite this course!'];  

        } else {

          $result = new FavoriteKnowledge;
          $result->Kno_ID = $id;
          $result->Use_ID = session('session_id');
          $result->Fav_IP = $_SERVER['REMOTE_ADDR'];
          $result->save();

        }
        
        return back()->with('success','Favorite success');

    }

    public function comment() {

        $this->validate(request(), [
        'detail' => 'required|string'
        ]);

        $result = new CommentKnowledge;
        $result->Kno_ID = $request->knowledge_id;
        $result->Use_ID = session('session_id');
        $result->Com_Detail = $request->detail;
        $result->Com_IP = $_SERVER['REMOTE_ADDR'];
        $result->save();

        return back()->with('success',trans('other.add_success'));

    }

}
