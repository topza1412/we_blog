<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\SendMail;
use App\Contact;
use App\Home;

class ContactController extends Controller
{
    public function index() {

      //seo
      $seo = $this->Seo(null,'Contact','Contact','Contact','Contact',url('contact'));

      $data = array('seo' => $seo);

      return view('page.member.contact',['data' => $data]);
    }

}
