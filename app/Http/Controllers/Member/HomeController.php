<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\VisitWeb;
use App\Knowledge;

class HomeController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }


    public function index()
    {   

        $device = $this->DeviceDetect();

        VisitWeb::AddVisit($device);

        $count_knowledge['core'] = Knowledge::where('Gro_ID', 1)->count();
        $count_knowledge['advance'] = Knowledge::where('Gro_ID', 2)->count();
        $count_knowledge['innovation'] = Knowledge::where('Gro_ID', 3)->count();

        $knowledge['core'] = Knowledge::join('admin', 'admin.Adm_ID', '=', 'knowledge.Adm_ID')
        ->join('categories', 'categories.Cat_ID', '=', 'knowledge.Cat_ID')
        ->where('knowledge.Gro_ID', 1)
        ->orderby('knowledge.Kno_ID', 'desc')
        ->limit(6)
        ->get();

        $knowledge['advance'] = Knowledge::join('admin', 'admin.Adm_ID', '=', 'knowledge.Adm_ID')
        ->join('categories', 'categories.Cat_ID', '=', 'knowledge.Cat_ID')
        ->where('knowledge.Gro_ID', 2)
        ->orderby('knowledge.Kno_ID', 'desc')
        ->limit(6)
        ->get();

        //seo
        $seo = $this->Seo(null,null,$this->SettingWeb()->Set_Description,$this->SettingWeb()->Set_Keywords,$this->SettingWeb()->Set_Robots,url(''));

        $data = array('seo' => $seo, 'result' => $knowledge, 'count_knowledge' => $count_knowledge);

        return view('page.member.index',['data' => $data]);
    }
}
