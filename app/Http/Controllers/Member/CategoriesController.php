<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Knowledge;
use App\Categories;

class CategoriesController extends Controller
{
    public function main($id) {

      $result = Knowledge::join('admin', 'admin.Adm_ID', '=', 'knowledge.Adm_ID')
      ->join('categories', 'categories.Cat_ID', '=', 'knowledge.Cat_ID')
      ->where('knowledge.Cat_ID', $id)
      ->orderby('knowledge.Kno_ID','desc')
      ->paginate(9);

      $categories = Categories::where('Gro_ID', $result[0]->Gro_ID)->get();

      $tag = json_decode($result[0]->Kno_Tags);

      if($tag){
        $releases = Knowledge::join('admin', 'admin.Adm_ID', '=', 'knowledge.Adm_ID')
        ->join('categories', 'categories.Cat_ID', '=', 'knowledge.Cat_ID')
        ->where('knowledge.Cat_ID', $id)
        ->where('knowledge.Kno_Tags', 'like', '%'. $tag[0] .'%')
        ->orderby('knowledge.Kno_ID','desc')
        ->limit(4)
        ->get();
      } else {
        $releases = [];
      }

      //seo
      $seo = $this->Seo(null,'Categories','Categories','Categories','Categories',url('category'));

      $data = array('result' => $result, 'categories' => $categories, 'releases' => $releases, 'seo' => $seo);

      return view('page.member.category',['data' => $data]);
    }

}
