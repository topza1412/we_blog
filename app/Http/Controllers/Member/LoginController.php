<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Session;
use App\User;

class LoginController extends Controller
{

    public function index () {

        //seo
        $seo = $this->Seo(null,'Login','Login','Login','Login',url('login'));

        $data = ['seo' => $seo];

        return view('page.member.login',['data' => $data]);
    }


    public function auth(Request $request){

        if($request->post()){

            $this->validate(request(), [
                'username' => 'required|string|max:255',
                'password' => 'required|string|min:6',
            ]);

            $result = User::where(['Use_Username' => $request->username, 'Use_Password' => md5($request->password)])->first();

            if($result){
              $data = User::find($result->Use_ID);
              session()->put('session_id',$data->Use_ID);
              session()->put('session_username',$data->Use_Username);
              session()->put('session_department',$data->Use_Department);
              session()->put('session_section',$data->Use_Section);
              if($request->remember_login!=null){
                    setcookie ("remember_login",$request->remember_login,time()+ (10 * 365 * 24 * 60 * 60));
                    setcookie ("remember_username",$request->username,time()+ (10 * 365 * 24 * 60 * 60));
                    setcookie ("remember_password",$request->password,time()+ (10 * 365 * 24 * 60 * 60));
              } else{
                    setcookie ("remember_login",null);
                    setcookie ("remember_username",null);
                    setcookie ("remember_password",null);    
              }
                return redirect('dashboard');

            } else if($result['result']=='permission failed'){

                return back()->with('error',trans('login.permission_failed'));

            } else{
                
                return back()->with('error',trans('login.login_failed'));   
            }

        }

    }

    public function logout(){
        Session::flush();
        return redirect('/');
    }


}
