<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Image;
use Session;
use App\Setting;
use App\Categories;
use App\Visit;
use App\SlideBanner;
use App\LogsSystem;
use App\LogsKnowledge;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //language
    public function LangDefault(){

        if(app()->getLocale()=='th'){
            $lang_id = 1;
        }
        else {
            $lang_id = 2;
        }

        return $lang_id;
    }


    //setting web
    public static function SettingWeb(){
        $result = Setting::all();
        return $result[0];
    }

    //status web
    public static function StatusWeb(){
        $result = Setting::all();
        if($result[0]->Set_StatusWeb==0){
          return false;
        }
        else{
          return true;
        }
    }

    //setting web
    public static function Seo($logo,$title,$description,$keywords,$robots,$url){

        $seo['logo_seo'] = $logo;
        $seo['title_seo'] = $title;
        $seo['description_seo'] = $description;
        $seo['keywords_seo'] = $keywords;
        $seo['robots_seo'] = $robots;
        $seo['url_seo'] = $url;

        return $seo;
    }


    //slide banner
    public static function SlideBanner(){
        $result = SlideBanner::where('Sli_Status',1)->get();
        return $result;
    }

    //category menu
     public static function CategoryMenu(){

      $result1 = Categories::where(['Gro_ID' => 1, 'Cat_Status' => 1])->get();
      $result2 = Categories::where(['Gro_ID' => 2, 'Cat_Status' => 1])->get();
      $result3 = Categories::where(['Gro_ID' => 3, 'Cat_Status' => 1])->get();

      $result = array ('category1' => $result1, 'category2' => $result3, 'category3' => $result3);

      return $result;
    }

    //check device visit web
    public static function DeviceDetect() {

      if(preg_match("/ipad/i", $_SERVER['HTTP_USER_AGENT'])){
        $device = 'iPad';
      }
      else if(preg_match("/tablet/i", $_SERVER['HTTP_USER_AGENT'])){
        $device = 'tablet';
      }
      else if(preg_match("/iphone/i", $_SERVER['HTTP_USER_AGENT'])){
        $device = 'iPhone';
      }
      else if(preg_match("/android/i", $_SERVER['HTTP_USER_AGENT'])){
        $device = 'Android';
      }
      else{
        $device = 'Desktop';
      }

    return $device;
    }


    //thai date
    public function DateThai($type,$strDate)
  {
    $strYear = date("Y",strtotime($strDate))+543;
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strHour= date("H",strtotime($strDate));
    $strMinute= date("i",strtotime($strDate));
    $strSeconds= date("s",strtotime($strDate));
    $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
    $strMonthThai=$strMonthCut[$strMonth];

    if($type=='month_year'){
    return "$strMonthThai $strYear";
    }
    else if($type=='year'){
    return "$strYear";
    }

  }


  //next date
  public static function NextDate($start,$next,$type){
   $strdate = explode("-",$start);
   switch($type) {
    case "D" :  return( date("Y-m-d", mktime(0, 0, 0, $strdate["1"], $strdate["2"]+$next,  $strdate["0"]))); break;
    case "M" :  return( date("Y-m-d", mktime(0, 0, 0, $strdate["1"]+$next, $strdate["2"],  $strdate["0"]))); break;
    case "Y" :  return( date("Y-m-d", mktime(0, 0, 0, $strdate["1"], $strdate["2"],  $strdate["0"]+$next))); break;
    default : return( date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")+7,  date("Y"))));
   }
}


  //upload file
  public static function Upload_File ($file_upload, $path, $width = 0, $height = 0){

    $filename  = Date("dmy_His").'.'.$file_upload->getClientOriginalExtension();
    $image_resize = Image::make($file_upload->getRealPath());              
    $image_resize->resize($width, $height);
    $image_resize->save(public_path($path.$filename));

    return $filename;

  }

  public static function LogsKnowledge ($knowledge_id) {

    $result = new LogsKnowledge;
    $result->Adm_ID = session('session_admin_id');
    $result->Kno_ID = $knowledge_id;
    $result->Log_IP = $_SERVER['REMOTE_ADDR'];
    $result->save();

    return $result;

  }

  public static function LogsSystem ($routes, $type, $detail) {

    $result = new LogsSystem;
    $result->Log_Name = $type.' data '.$routes;
    $result->Log_Detail = $detail;
    $result->Log_UrlAccess = $_SERVER['HTTP_REFERER'];
    $result->Log_Browser = $_SERVER['HTTP_USER_AGENT'];
    $result->Log_IP = $_SERVER['REMOTE_ADDR'];
    $result->Adm_ID = session('session_admin_id');
    $result->save();

    return $result;

  }

    
}
