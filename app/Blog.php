<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model {
 
     protected $table = 'blog';
 
     protected $primaryKey = 'Blo_ID';

     public $timestamps = true;

}

