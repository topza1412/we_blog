<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class SlideBanner extends Model {
 
     protected $table = 'slider';
 
     protected $primaryKey = 'Sli_ID';

 	public $timestamps = true;
}

