<?php

namespace App;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;

class User extends Model {
 
     protected $table = 'user';
 
     protected $primaryKey = 'Use_ID';

     public $timestamps = true;
 
}

